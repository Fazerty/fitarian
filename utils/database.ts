import 'reflect-metadata';
import { Alim, AlimGrp, Compo, Const, Source, User, WeightInfo, ActivityInfo, Ingredient, Menu } from '../entities'

import { Connection, createConnection } from 'typeorm/browser';
// import { LoggerOptions } from 'typeorm/logger/LoggerOptions';
import { PostRefactoring1587394503156 } from './database/migrations';

// const logOptions: LoggerOptions = 'all';

export function connect(): Promise<Connection> {
  return createConnection({
    database: 'fitariandb',
    driver: require('expo-sqlite'),
    entities: [Alim, AlimGrp, Compo, Const, Source, User, WeightInfo, ActivityInfo, Ingredient, Menu],
    synchronize: true,
    logger: 'advanced-console',
    // logging: logOptions,
    type: 'expo',
    migrationsTableName: 'custom_migration',
    migrations: [PostRefactoring1587394503156],
  });


}