import {MigrationInterface, QueryRunner} from "typeorm";

export class PostRefactoring1587394503156 implements MigrationInterface {
    name = 'PostRefactoring1587394503156'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "menu" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "ingredient" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "quantityValue" integer NOT NULL, "quantityUnit" varchar NOT NULL, "menuId" integer, "alimId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "alim" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "alim_code" integer, "alim_nom_fr" varchar, "alim_nom_index_fr" varchar, "alim_nom_eng" varchar, "alim_nom_index_eng" varchar, "alim_grp_code" integer, "alim_ssgrp_code" integer, "alim_ssssgrp_code" integer, "alimGrpId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "alim_grp" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "grp_code" integer, "alim_grp_code" integer, "alim_grp_nom_fr" varchar, "alim_grp_nom_eng" varchar, "alim_ssgrp_code" integer, "alim_ssgrp_nom_fr" varchar, "alim_ssgrp_nom_eng" varchar, "alim_ssssgrp_code" integer, "alim_ssssgrp_nom_fr" varchar, "alim_ssssgrp_nom_eng" varchar)`, undefined);
        await queryRunner.query(`CREATE TABLE "compo" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "alim_code" integer, "const_code" integer, "teneur" varchar, "min" varchar, "max" varchar, "code_confiance" varchar, "source_code" integer, "alimId" integer, "constId" integer, "sourceId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "const" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "const_code" integer, "const_nom_fr" varchar, "const_nom_eng" varchar)`, undefined);
        await queryRunner.query(`CREATE TABLE "source" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "source_code" integer, "ref_citation" varchar)`, undefined);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "formula" varchar, "dateOfBirth" datetime, "sex" varchar, "heightValue" integer, "heightUnit" varchar, "expectedWeightValue" integer, "expectedWeightUnit" varchar)`, undefined);
        await queryRunner.query(`CREATE TABLE "weight_info" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "weightValue" integer NOT NULL, "weightUnit" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "activity_info" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "activity" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_menu" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer, CONSTRAINT "FK_4567d47472c13f6d5145c0443a2" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_menu"("id", "title", "date", "userId") SELECT "id", "title", "date", "userId" FROM "menu"`, undefined);
        await queryRunner.query(`DROP TABLE "menu"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_menu" RENAME TO "menu"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_ingredient" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "quantityValue" integer NOT NULL, "quantityUnit" varchar NOT NULL, "menuId" integer, "alimId" integer, CONSTRAINT "FK_67f1763bf7f8850a795adca7550" FOREIGN KEY ("menuId") REFERENCES "menu" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_e1fe6c51bde1d513c590a3830bc" FOREIGN KEY ("alimId") REFERENCES "alim" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_ingredient"("id", "quantityValue", "quantityUnit", "menuId", "alimId") SELECT "id", "quantityValue", "quantityUnit", "menuId", "alimId" FROM "ingredient"`, undefined);
        await queryRunner.query(`DROP TABLE "ingredient"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_ingredient" RENAME TO "ingredient"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_alim" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "alim_code" integer, "alim_nom_fr" varchar, "alim_nom_index_fr" varchar, "alim_nom_eng" varchar, "alim_nom_index_eng" varchar, "alim_grp_code" integer, "alim_ssgrp_code" integer, "alim_ssssgrp_code" integer, "alimGrpId" integer, CONSTRAINT "FK_71d0edbbd887c9296a097084954" FOREIGN KEY ("alimGrpId") REFERENCES "alim_grp" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_alim"("id", "alim_code", "alim_nom_fr", "alim_nom_index_fr", "alim_nom_eng", "alim_nom_index_eng", "alim_grp_code", "alim_ssgrp_code", "alim_ssssgrp_code", "alimGrpId") SELECT "id", "alim_code", "alim_nom_fr", "alim_nom_index_fr", "alim_nom_eng", "alim_nom_index_eng", "alim_grp_code", "alim_ssgrp_code", "alim_ssssgrp_code", "alimGrpId" FROM "alim"`, undefined);
        await queryRunner.query(`DROP TABLE "alim"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_alim" RENAME TO "alim"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_compo" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "alim_code" integer, "const_code" integer, "teneur" varchar, "min" varchar, "max" varchar, "code_confiance" varchar, "source_code" integer, "alimId" integer, "constId" integer, "sourceId" integer, CONSTRAINT "FK_947b7704b3579fd4f421781613c" FOREIGN KEY ("alimId") REFERENCES "alim" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_2dce0ee6be6eae10bf8a8e8b12a" FOREIGN KEY ("constId") REFERENCES "const" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_42f04342cb61187387f06aebc92" FOREIGN KEY ("sourceId") REFERENCES "source" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_compo"("id", "alim_code", "const_code", "teneur", "min", "max", "code_confiance", "source_code", "alimId", "constId", "sourceId") SELECT "id", "alim_code", "const_code", "teneur", "min", "max", "code_confiance", "source_code", "alimId", "constId", "sourceId" FROM "compo"`, undefined);
        await queryRunner.query(`DROP TABLE "compo"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_compo" RENAME TO "compo"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_weight_info" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "weightValue" integer NOT NULL, "weightUnit" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer, CONSTRAINT "FK_25e8b01ae4de76018015a9d79ad" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_weight_info"("id", "weightValue", "weightUnit", "date", "userId") SELECT "id", "weightValue", "weightUnit", "date", "userId" FROM "weight_info"`, undefined);
        await queryRunner.query(`DROP TABLE "weight_info"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_weight_info" RENAME TO "weight_info"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_activity_info" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "activity" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer, CONSTRAINT "FK_a4a87d4de7f7e1a3fdbe08cb51b" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_activity_info"("id", "activity", "date", "userId") SELECT "id", "activity", "date", "userId" FROM "activity_info"`, undefined);
        await queryRunner.query(`DROP TABLE "activity_info"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_activity_info" RENAME TO "activity_info"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "activity_info" RENAME TO "temporary_activity_info"`, undefined);
        await queryRunner.query(`CREATE TABLE "activity_info" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "activity" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "activity_info"("id", "activity", "date", "userId") SELECT "id", "activity", "date", "userId" FROM "temporary_activity_info"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_activity_info"`, undefined);
        await queryRunner.query(`ALTER TABLE "weight_info" RENAME TO "temporary_weight_info"`, undefined);
        await queryRunner.query(`CREATE TABLE "weight_info" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "weightValue" integer NOT NULL, "weightUnit" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "weight_info"("id", "weightValue", "weightUnit", "date", "userId") SELECT "id", "weightValue", "weightUnit", "date", "userId" FROM "temporary_weight_info"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_weight_info"`, undefined);
        await queryRunner.query(`ALTER TABLE "compo" RENAME TO "temporary_compo"`, undefined);
        await queryRunner.query(`CREATE TABLE "compo" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "alim_code" integer, "const_code" integer, "teneur" varchar, "min" varchar, "max" varchar, "code_confiance" varchar, "source_code" integer, "alimId" integer, "constId" integer, "sourceId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "compo"("id", "alim_code", "const_code", "teneur", "min", "max", "code_confiance", "source_code", "alimId", "constId", "sourceId") SELECT "id", "alim_code", "const_code", "teneur", "min", "max", "code_confiance", "source_code", "alimId", "constId", "sourceId" FROM "temporary_compo"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_compo"`, undefined);
        await queryRunner.query(`ALTER TABLE "alim" RENAME TO "temporary_alim"`, undefined);
        await queryRunner.query(`CREATE TABLE "alim" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "alim_code" integer, "alim_nom_fr" varchar, "alim_nom_index_fr" varchar, "alim_nom_eng" varchar, "alim_nom_index_eng" varchar, "alim_grp_code" integer, "alim_ssgrp_code" integer, "alim_ssssgrp_code" integer, "alimGrpId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "alim"("id", "alim_code", "alim_nom_fr", "alim_nom_index_fr", "alim_nom_eng", "alim_nom_index_eng", "alim_grp_code", "alim_ssgrp_code", "alim_ssssgrp_code", "alimGrpId") SELECT "id", "alim_code", "alim_nom_fr", "alim_nom_index_fr", "alim_nom_eng", "alim_nom_index_eng", "alim_grp_code", "alim_ssgrp_code", "alim_ssssgrp_code", "alimGrpId" FROM "temporary_alim"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_alim"`, undefined);
        await queryRunner.query(`ALTER TABLE "ingredient" RENAME TO "temporary_ingredient"`, undefined);
        await queryRunner.query(`CREATE TABLE "ingredient" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "quantityValue" integer NOT NULL, "quantityUnit" varchar NOT NULL, "menuId" integer, "alimId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "ingredient"("id", "quantityValue", "quantityUnit", "menuId", "alimId") SELECT "id", "quantityValue", "quantityUnit", "menuId", "alimId" FROM "temporary_ingredient"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_ingredient"`, undefined);
        await queryRunner.query(`ALTER TABLE "menu" RENAME TO "temporary_menu"`, undefined);
        await queryRunner.query(`CREATE TABLE "menu" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "date" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "menu"("id", "title", "date", "userId") SELECT "id", "title", "date", "userId" FROM "temporary_menu"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_menu"`, undefined);
        await queryRunner.query(`DROP TABLE "activity_info"`, undefined);
        await queryRunner.query(`DROP TABLE "weight_info"`, undefined);
        await queryRunner.query(`DROP TABLE "user"`, undefined);
        await queryRunner.query(`DROP TABLE "source"`, undefined);
        await queryRunner.query(`DROP TABLE "const"`, undefined);
        await queryRunner.query(`DROP TABLE "compo"`, undefined);
        await queryRunner.query(`DROP TABLE "alim_grp"`, undefined);
        await queryRunner.query(`DROP TABLE "alim"`, undefined);
        await queryRunner.query(`DROP TABLE "ingredient"`, undefined);
        await queryRunner.query(`DROP TABLE "menu"`, undefined);
    }

}
