
import { ApolloClient } from 'apollo-client';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import fetch from 'node-fetch';
import { HttpLink } from 'apollo-link-http';

type TApolloClient = ApolloClient<NormalizedCacheObject>;

let globalApolloClient: TApolloClient;

/**
 * Always creates a new apollo client on the server
 * Creates or reuses apollo client in the browser.
 * @param  {Object} initialState
 */
export function initApolloClient(initialState?: any) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (typeof window === 'undefined') {
    return createApolloClient(initialState);
  }
  // Reuse client on the client-side
  if (!globalApolloClient) {
    globalApolloClient = createApolloClient(initialState);
  }
  return globalApolloClient;
}

/**
 * Creates and configures the ApolloClient
 * @param  {Object} [initialState={}]
 */
function createApolloClient(initialState = {}) {
  const ssrMode = typeof window === 'undefined';
  const cache = new InMemoryCache().restore(initialState);

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  return new ApolloClient({
    ssrMode,
    link: createIsomorphLink(),
    cache,
  });
}
/**
 * the port of the express server
 */
const port = parseInt(process.env.PORT || '3000', 10);
/**
 * Is it the dev env?
 */
// const _dev = process.env.NODE_ENV !== 'production'

/**
 * the path of the apollo/graphql server
 */
const graphqlPath: string = '/api/graphql';

/**
 *
 *
 * @returns
 */
function createIsomorphLink() {
  // https://bitbucket.org/Fazerty/fitarian-back.git
  const uri: string = 'https://fitarian.upurion.com' + graphqlPath
  return new HttpLink({
    uri,
    credentials: 'omit',
    fetch: fetch,
  });
}



export const apolloState = {};