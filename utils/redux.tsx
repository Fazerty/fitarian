import { configureStore, AppState as State } from '../store/store'
import { Store } from 'redux';

export const reduxStore: Store<State, any> = configureStore()

