import {
  Entity, PrimaryGeneratedColumn, Column, ManyToOne
} from 'typeorm';
import { User } from '.';

export type WeightUnit = 'kg' | 'lbs';

/**
* The data-sources which were used to produce published data of the version 2017 of the ANSES-CIQUAL
table are detailed in the file sources_2017 11 21.xml.
*
* @export
* @class Source
*/
@Entity({name:'weight_info'})
export class WeightInfo {

  @PrimaryGeneratedColumn()
  public id?: number;

  @ManyToOne(_type => User, user => user.weightInfos)
  user?: User;

  /**
   *
   *
   * @type {number}
   * @memberof WeightInfo
   */
  @Column()
  weightValue?: number;

  /**
   *
   *
   * @type {string}
   * @memberof WeightInfo
   */
  @Column({ type: 'varchar' })
  weightUnit?: WeightUnit;

  /**
   *
   *
   * @type {Date}
   * @memberof WeightInfo
   */
  @Column()
  date?: Date;

}

export default WeightInfo;