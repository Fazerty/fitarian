import {
  Entity, PrimaryGeneratedColumn, Column, ManyToOne
} from 'typeorm';
import { User } from '.';
import { ActivityType } from '../store/system/types';


/**
* The data-sources which were used to produce published data of the version 2017 of the ANSES-CIQUAL
* table are detailed in the file sources_2017 11 21.xml.
*
* @export
* @class Source
*/
@Entity({name:'activity_info'})
export class ActivityInfo {

  @PrimaryGeneratedColumn()
  public id?: number;

  /**
   *
   *
   * @type {User}
   * @memberof ActivityInfo
   */
  @ManyToOne(_type => User, user => user.activityInfos)
  user!: User;

  /**
   *
   *
   * @type {number}
   * @memberof WeightInfo
   */
  @Column({ type: 'varchar' })
  activity!: ActivityType;

  /**
   *
   *
   * @type {Date}
   * @memberof WeightInfo
   */
  @Column()
  date!: Date;

}

export default ActivityInfo;