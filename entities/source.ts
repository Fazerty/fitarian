import {
    Entity, PrimaryGeneratedColumn, Column, OneToMany
} from 'typeorm';
import { Compo } from '.';

/**
 * The data-sources which were used to produce published data of the version 2017 of the ANSES-CIQUAL
table are detailed in the file sources_2017 11 21.xml.
 *
 * @export
 * @class Source
 */
@Entity({name:'source'})
export class Source {

    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     * code of data-sources
     *
     * @type {number}
     * @memberof Source
     */
    @Column({ nullable: true })
    public source_code?: number;

    /**
     * name of data-sources
     *
     * @type {string}
     * @memberof Source
     */
    @Column({ nullable: true })
    public ref_citation?: string;

    /**
     *  The compo using this source.
     *
     * @type {Compo[]}
     * @memberof Source
     */
    @OneToMany(_type => Compo, compo => compo.source)
    public compos?: Compo[];
}

export default Source;