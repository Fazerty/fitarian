import { OneToMany, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { WeightInfo, WeightUnit } from '.';
import Menu from './menu';
import ActivityInfo from './activityInfo';

export type HeightUnit = 'cm' | 'inch';

export type Sex = 'm' | 'f';

/**
 *
 * @export
 * @class User
 */
@Entity({name:'user'})
export class User {

  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ nullable: true })
  name?: string

  /**
   * The formula used to calculate the kcal, kj ???
   *
   * @type {string}
   * @memberof User
   */
  @Column({ nullable: true })
  formula?: string

  @Column({ nullable: true })
  dateOfBirth?: Date;

  @Column({ type: 'varchar', nullable: true })
  sex?: Sex;

  @Column({ nullable: true })
  heightValue?: number;

  @Column({ type: 'varchar', nullable: true })
  heightUnit?: HeightUnit;

  @Column({ nullable: true })
  expectedWeightValue?: number;

  @Column({ type: 'varchar', nullable: true })
  expectedWeightUnit?: WeightUnit;

  /**
   * The activity of the user at different moments.
   *
   * @type {ActivityInfo[]}
   * @memberof User
   */
  @OneToMany(_type => ActivityInfo, activityInfo => activityInfo.user)
  activityInfos?: ActivityInfo[];

  /**
   * The weights of the user at different moments.
   *
   * @type {WeightInfo[]}
   * @memberof User
   */
  @OneToMany(_type => WeightInfo, weightInfo => weightInfo.user)
  weightInfos?: WeightInfo[];

  /**
   * The daily menu of the user at different moments.
   *
   * @type {WeightInfo[]}
   * @memberof User
   */
  @OneToMany(_type => Menu, menu => menu.user)
  menus?: Menu[];

}

export default User;
