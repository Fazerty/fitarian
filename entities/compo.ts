import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Alim, Const, Source } from '.';

/**
 * The food composition data of 2017 version of ANSES-CIQUAL table is available in the file
compo_2017 11 21.xml. Whenever possible, a value is given for the pair [food, component] (the food and
the component are described in the files alim_2017 11 21.xml and const_2017 11 21.xml).
 *
 * @export
 * @class Compo
 */
@Entity({name:'compo'})
export class Compo {

    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     * code of the food
     *
     * @type {number}
     * @memberof Compo
     */
    @Column({ nullable: true })
    public alim_code?: number;


    /**
     *  The alim in this compo.
     *
     * @type {Alim}
     * @memberof Compo
     */
    @ManyToOne(_type => Alim, alim => alim.compos)
    public alim?: Alim;


    /**
     * code of the component
     *
     * @type {number}
     * @memberof Compo
     */
    @Column({ nullable: true })
    public const_code?: number;


    @ManyToOne(_type => Const, cnst => cnst.compos, { eager: true, cascade : ['insert']})
    public const?: Const;

    /**
     * it can be a value, a max value (example : '<10'), the indication
     * 'trace' or a hyphen if the value is missing
     *
     * @type {string}
     * @memberof Compo
     */
    @Column({ nullable: true })
    public teneur?: string;

    /**
     * min minimum value observed in the data-sources
     *
     * @type {string}
     * @memberof Compo
     */
    @Column({ nullable: true })
    min?: string;

    /**
     * max maximum value observed in the data-sources
     *
     * @type {string}
     * @memberof Compo
     */
    @Column({ nullable: true })
    max?: string;

    /**
     * code_confiance confidence code, which characterizes the quality of the average
     * content_value(A = very reliable to D = less reliable)
     *
     * @type {string}
     * @memberof Compo
     */
    @Column({ nullable: true })
    code_confiance?: string;


    /**
     *
    source_code code of the data-sources
     *
     * @type {number}
     * @memberof Compo
     */
    @Column({ nullable: true })
    source_code?: number;

    /**
     * The source
     *
     * @type {Source}
     * @memberof Compo
     */
    @ManyToOne(_type => Source, source => source.compos, { eager: true, cascade : ['insert']})
    public source?: Source;
}

export default Compo;