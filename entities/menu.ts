import {
  Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany
} from 'typeorm';
import { User } from '.';
import Ingredient from './ingredient';


/**
* The daily menu of a user. What he eats in a day.
*
* @export
* @class Source
*/
@Entity({name:'menu'})
export class Menu {

  @PrimaryGeneratedColumn()
  public id?: number;

/**
 * The title of the menu
 *
 * @type {string}
 * @memberof Menu
 */
  @Column({ nullable: false })
  public title?: string;

  @ManyToOne(_type => User, user => user.menus)
  user!: User;

  /**
   *
   *
   * @type {number}
   * @memberof Menu
   */
  @OneToMany(_type => Ingredient, ingredient => ingredient.menu, {cascade: true})
  ingredients!: Ingredient[];

  /**
   *
   *
   * @type {Date}
   * @memberof Menu
   */
  @Column()
  date?: Date;

}

export default Menu;