import {
  Entity, PrimaryGeneratedColumn, Column, ManyToOne
} from 'typeorm';
import Menu from './menu';
import Alim from './alim';
import Compo from './compo';

type quantityUnit = 'g' | 'oz'

/**
* The ingredient of a menu: the quantity of an alim.
*
* @export
* @class Source
*/
@Entity({name:'ingredient'})
export class Ingredient {

  @PrimaryGeneratedColumn()
  public id?: number;

  /*
        const_code: 327 => Energy, Regulation EU No 1169/2011 (kJ/100g)
        const_code: 328 => Energy, Regulation EU No 1169/2011 (kcal/100g)
        const_code: 332  => Energy, N x Jones&apos; factor, with fibres (kJ/100g)
        const_code: 333 => Energy, N x Jones&apos; factor, with fibres  (kcal/100g)
  */
  /**
   * Returns the kcal for a given amount for alim.
   * Params quantityValue & quantityUnit are used for the calculation if given, if not the quantityValue & quantityUnit of the object are used.
   *
   * @param {(number | undefined)} quantityValue
   * @param {('g' | 'oz' | undefined)} quantityUnit
   * @returns {(number | undefined)}
   * @memberof Ingredient
   */
  getKCal(quantityValue?: number, quantityUnit?: 'g' | 'oz'): number | undefined {

    quantityValue = quantityValue ? quantityValue : this.quantityValue;
    quantityUnit = quantityUnit ? quantityUnit : this.quantityUnit;

    if (this.alim && this.alim.compos) {
      const kcalCompo = this.alim.compos.filter((compo: Compo) => {
        if (compo.const_code === 328) { return true; };
      })
      if (kcalCompo.length === 1) {
        const kcalPer100g: number = parseFloat(kcalCompo[0].teneur ? kcalCompo[0].teneur : '');
        if (Number.isNaN(kcalPer100g)) { return undefined }
        // Convert from oz to g if necessary.
        const g = !quantityValue ? 0 : quantityUnit === 'g' ? quantityValue : quantityValue * 28.349523;
        const kcal = g === 0 ? 0 : (!kcalPer100g ? 0 : (kcalPer100g / 100 * g));
        const rounded: number = Math.round(kcal);
        if (Number.isNaN(rounded)) { return undefined } else { return Math.round(kcal); }
      }
    }
    return undefined;
  }

  constructor(alim: Alim) {
    this.alim = alim;
    this.quantityValue = 0;
    this.quantityUnit = 'g';
  }

  @ManyToOne(_type => Menu, menu => menu.ingredients)
  menu!: Menu;

  /**
   *
   *
   * @type {number}
   * @memberof Ingredient
   */
  @ManyToOne(_type => Alim, alim => alim.ingredients, { eager: true, cascade: ['insert'] })
  alim!: Alim;

  /**
   *
   *
   * @type {number}
   * @memberof Ingredient
   */
  @Column()
  quantityValue?: number;

  /**
   *
   *
   * @type {string}
   * @memberof Ingredient
   */
  @Column({ type: 'varchar' })
  quantityUnit?: quantityUnit;

}

export default Ingredient;