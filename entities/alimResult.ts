
import { Alim } from '.';

export class AlimResult {

  /**
   * the total number of results
   *
   * @type {number}
   * @memberof Alim
   */
  public count: number;

  /**
   * the alims
   *
   * @type {number}
   * @memberof Alim
   */
  public result: Partial<Alim>[];


  constructor(results: [Alim[], number]) {
    this.count = results[1];
    this.result = results[0];
  }

}