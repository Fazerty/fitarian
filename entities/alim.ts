import {
    Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany
} from 'typeorm';
import { AlimGrp, Compo } from '.';
import Ingredient from './ingredient';

/**
 * The foods of the version 2017 of the ANSES-CIQUAL food composition table are listed in the file
 * alim_2017 11 21.xml.
 * Each food is identified by its code (alim_code) and has a name in French (alim_nom_fr) and in English
 * (alim_nom_eng). The group, subgroup and sub-subgroup codes refer to the file alim_grp_2017 11 21.xml
 * which is described in alminGrp.
 *
 *
 * @export
 * @class Alim
 */
@Entity({name:'alim'})
export class Alim {


    @PrimaryGeneratedColumn()
    public id?: number;

    /**
     * code of the food
     *
     * @type {number}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_code?: number;

    /**
     * name of the food in French
     *
     * @type {string}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_nom_fr?: string;

    /**
     * ??
     *
     * @type {string}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_nom_index_fr?: string;

    /**
     * name of the food in English
     *
     * @type {string}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_nom_eng?: string;

    /**
     * ??
     *
     * @type {string}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_nom_index_eng?: string;

    /**
     * code of the food group
     *
     * @type {number}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_grp_code?: number;

    /**
     * the food group (or subgroup, or sub subgroup)
     *  Rem: This classification of groups is a bit strange.
     *  Have to link the alim to a group having code
     *  - the alim_grp_code if it's a group (with alim_ssgrp_code)
     *  - the alim_ssgrp_code if it's a subgroup (with alim_ssssgrp_code)
     *  - the alim_ssssgrp_code if it's a sub subgroup
     *
     * @type {AlimGrp}
     * @memberof Alim
     */
    @ManyToOne(_type => AlimGrp, alim_grp => alim_grp.alims, {cascade : ['insert']})
    public alim_grp?: AlimGrp;

    /**
     *  The composants of this aliment.
     *
     * @type {Alim}
     * @memberof Compo
     */
    @OneToMany(_type => Compo, compo => compo.alim, {eager: true, cascade : ['insert']})
    public compos?: Compo[];

    /**
     * code of the food subgroup
     *
     * @type {number}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_ssgrp_code?: number;


    /**
     * code of the food sub-subgroup
     *
     * @type {number}
     * @memberof Alim
     */
    @Column({ nullable: true })
    public alim_ssssgrp_code?: number;

    /**
     * Ingredients in which the alim is used.
     *
     * @type {Ingredient[]}
     * @memberof Alim
     */
    @OneToMany(_type => Ingredient, ingredient => ingredient.alim )
    ingredients!: Ingredient[]

}

export default Alim;
