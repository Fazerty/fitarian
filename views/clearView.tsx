import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';
import { AppState } from '../store/store';
import { connect } from 'react-redux';
import { SystemState } from '../store/system/types';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { clearAllData } from '../store/system/actions';

interface ClearDataProps {
  clearAllData: typeof clearAllData;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class ClearData extends Component<ClearDataProps> {

  clearData() {
    this.props.clearAllData();
    this.props.navigation.navigate('Params', { screen: 'Welcome' });
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>{this.props.system.language === 'en' ? 'Are you sure?' : 'Es-tu certain(e)?'}</Text>
        <Text>
          {this.props.system.language === 'en'
            ? 'Do you want to clear all data?'
            : 'Veux-tu effacer toute tes données?'}
        </Text>
        <Button
          title="Ok"
          onPress={() => this.clearData()}
        />
      </View>
    );
  }
}

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const ClearDataRedux = connect(mapStateToProps, {clearAllData})(ClearData);

export default ClearDataRedux;
