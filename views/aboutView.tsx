import React, { Component } from 'react';
import { Text, View, Button, Linking } from 'react-native';
import { AppState } from '../store/store';
import { connect } from 'react-redux';
import { SystemState } from '../store/system/types';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { clearAllData } from '../store/system/actions';
import { H2 } from 'native-base';

interface AboutProps {
  clearAllData: typeof clearAllData;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class About extends Component<AboutProps> {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          maxHeight: 600,
          paddingTop: 60,
        }}
      >
        <H2 style={{ paddingBottom: 30 }}>
          {this.props.system.language === 'en'
            ? 'Fitarian mobile application'
            : "L'application mobile fitarian"}
        </H2>

        {this.props.system.language === 'en' ? (
          <View style={{ }}>
            <Text>This project was made for my portfolio.</Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() => Linking.openURL('https://portfolio.upurion.com')}
            >
              https://portfolio.upurion.com
            </Text>
            <Text></Text>
            <Text>It's free an open source.</Text>
            <Text>The source code is available at </Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() =>
                Linking.openURL('https://bitbucket.org/Fazerty/fitarian')
              }
            >
              bitbucket
            </Text>
          </View>
        ) : (
          <View style={{  }}>
            <Text>Ce project a été conçu pour mon portfolio:</Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() => Linking.openURL('https://portfolio.upurion.com')}
            >
              https://portfolio.upurion.com
            </Text>
            <Text></Text>
            <Text>Il est gratuit et open source.</Text>
            <Text>The source code is available at</Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() =>
                Linking.openURL('https://bitbucket.org/Fazerty/fitarian')
              }
            >
              bitbucket
            </Text>
          </View>
        )}

        {this.props.system.language === 'en' ? (
          <View style={{ paddingBottom: 30 }}>
            <Text>This mobile app uses the fitarian server.</Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() => Linking.openURL('https://fitarian.upurion.com')}
            >
              https://fitarian.upurion.com
            </Text>
            <Text>The source code is available at </Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() =>
                Linking.openURL('https://bitbucket.org/Fazerty/fitarian-back')
              }
            >
              bitbucket
            </Text>
          </View>
        ) : (
          <View style={{  paddingBottom: 30 }}>
            <Text>Ce application mobile utilise le serveur fitarian:</Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() => Linking.openURL('https://fitarian.upurion.com')}
            >
              https://fitarian.upurion.com
            </Text>
            <Text>The source code is available at</Text>
            <Text
              style={{ fontWeight: 'bold', color: 'blue' }}
              onPress={() =>
                Linking.openURL('https://bitbucket.org/Fazerty/fitarian-back')
              }
            >
              bitbucket
            </Text>
          </View>
        )}
        <View
          style={{
            borderBottomColor: 'black',
            borderBottomWidth: 1,
          }}
        />
        {this.props.system.language === 'en' ? (
          <View style={{ paddingBottom: 30 }}>
            <Text style={{ fontWeight: 'bold' }}>
              The database used is from Ciqual
            </Text>
            <Text style={{ color: 'gray' }}>
              {'The present data and information are made available to the public by ' +
                'the French Agency for Food, Environmental and Occupational Health & ' +
                'Safety (ANSES). \n\n' +
                'They must not be reproduced in any form without clear ' +
                'indication of the source: "ANSES-CIQUAL French food composition ' +
                'table version 2017 ' +
                'or, in more detail, for a scientific publication: " French Agency ' +
                'for Food, Environmental and Occupational Health & Safety. \n\n' +
                'ANSES-CIQUAL French food composition table version 2017. ' +
                'The Ciqual homepage https://ciqual.anses.fr/ '}
            </Text>
          </View>
        ) : (
          <View style={{ paddingBottom: 30 }}>
            <Text style={{ fontWeight: 'bold' }}>
              La bases de données vient de Ciqual
            </Text>
            <Text style={{ color: 'gray' }}>
              {'La réutilisation des informations mises en ligne ' +
                'sur le site Anses-Ciqual est soumise à la condition ' +
                'que ces dernières ne soient pas altérées, que leur ' +
                'sens ne soit pas dénaturé et que la source ' +
                'ainsi que la version soient mentionnées, conformément aux ' +
                'dispositions de la loi n° 2016-1321 du 7 octobre 2016 pour une ' +
                'République numérique, et du code des relations entre le public et ' +
                "l'administration. \n\n" +
                'Les données de la table Ciqual sont ouvertes au public et ' +
                'téléchargeables gratuitement  via le portail ' +
                'data.gouv.fr ou depuis le site ' +
                'internet Ciqual https://ciqual.anses.fr/ "'}
            </Text>
          </View>
        )}
        <Button title="Back" onPress={() => this.props.navigation.goBack()} />
      </View>
    );
  }
}

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const AboutRedux = connect(mapStateToProps, { clearAllData })(About);

export default AboutRedux;
