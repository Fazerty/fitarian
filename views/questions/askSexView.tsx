import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Sex } from '../../entities/user';
import { SystemState } from '../../store/system/types';
import { updateSex } from '../../store/system/actions';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { Card, CardItem, H1, H2, Grid, Col, Icon, Button } from 'native-base';

interface Props {
  updateSex: typeof updateSex;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class AskSex extends Component<Props> {
  /**
   * Stores the sex and goes to the next question page.
   *
   * @param {Sex} sex
   * @memberof AskSex
   */
  setSex(sex: Sex) {
    this.props.updateSex(sex);
    this.props.navigation.navigate('Dob');
  }

  render() {
    return (
      <Card
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'lightGreen',
        }}
      >
        <CardItem style={{ backgroundColor: 'lightGreen' }}>
          <H1>
            {this.props.system.userName +
              (this.props.system.language === 'en'
                ? ', I need to know'
                : ", J'ai besoin de savoir")}
          </H1>
        </CardItem>
        <CardItem style={{ backgroundColor: 'lightGreen' }}>
          <H2>
            {this.props.system.language === 'en' ? 'your sexe' : 'ton sexe'}
          </H2>
        </CardItem>

        <CardItem style={{ backgroundColor: 'lightGreen' }}>
          <Grid>
            <Col>
              <Button
                style={{
                  backgroundColor: '#FFF7F8',
                  height: 200,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                activeOpacity={0.5}
                onPress={() => this.setSex('f')}
              >
                <Icon
                  type="FontAwesome"
                  name="female"
                  style={{ fontSize: 100, color: 'pink' }}
                />
              </Button>
            </Col>
            <Col>
              <Button
                style={{
                  backgroundColor: '#E3EEFF',
                  height: 200,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                activeOpacity={0.5}
                onPress={() => this.setSex('m')}
              >
                <Icon
                  type="FontAwesome"
                  name="male"
                  style={{ fontSize: 100, color: 'blue' }}
                />
              </Button>
            </Col>
          </Grid>
        </CardItem>
      </Card>
    );
  }
}

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskSexRedux = connect(mapStateToProps, { updateSex })(AskSex);

export default AskSexRedux;
