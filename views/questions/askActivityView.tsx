import React, { Component } from 'react';
import { SystemState, ActivityType } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateActivity } from '../../store/system/actions';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import {
  ListItem,
  Body,
  Text,
  CheckBox,
  Button,
  Card,
  CardItem,
  H2,
} from 'native-base';
import { StyleSheet, ImageBackground } from 'react-native';

interface Props {
  updateActivity: typeof updateActivity;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class AskActivity extends Component<Props> {
  state: { activity: ActivityType } = {
    activity: this.props.system.activity
      ? this.props.system.activity
      : 'sedentary',
  };

  /**
   * Stores the activity and goes to the next question page.
   *
   * @memberof AskActivity
   */
  setActivity() {
    const activity: ActivityType = this.state.activity;
    this.props.updateActivity(activity);
    this.props.navigation.navigate('Formula');
  }

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        source={require('../../assets/ryoji-iwata-n31JPLu8_Pw-unsplash.jpg')}
        resizeMode="cover"
      >
        <Card
          style={{
            width: 300,
          }}
        >
          <CardItem>
            <H2>
              {this.props.system.language === 'en'
                ? 'What is your activity?'
                : 'Quelle est votre activité?'}
            </H2>
          </CardItem>
          <ListItem>
            <CheckBox
              checked={this.state.activity === 'sedentary'}
              onPress={() => this.setState({ activity: 'sedentary' })}
            />
            <Body>
              <Text>
                {this.props.system.language === 'en'
                  ? 'Sedentary\n(little or no exercise)'
                  : "Sédentaire\n(peu ou pas d'exercice)"}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <CheckBox
              checked={this.state.activity === 'light'}
              onPress={() => this.setState({ activity: 'light' })}
            />
            <Body>
              <Text>
                {this.props.system.language === 'en'
                  ? 'Light\n(exercise 1-3 times a week)'
                  : 'Léger\n(exercice 1-3 fois par semaine)'}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <CheckBox
              checked={this.state.activity === 'moderate'}
              onPress={() => this.setState({ activity: 'moderate' })}
            />
            <Body>
              <Text>
                {this.props.system.language === 'en'
                  ? 'Moderate\n(exercise 3-5 times a week)'
                  : 'Modéré\n(exercice 3-5 fois par semaine)'}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <CheckBox
              checked={this.state.activity === 'high'}
              onPress={() => this.setState({ activity: 'high' })}
            />
            <Body>
              <Text>
                {this.props.system.language === 'en'
                  ? 'High\n(heavy exercise 6-7 times a week)'
                  : 'Élevé\n(exercice intensif 6-7 fois par semaine)'}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <CheckBox
              checked={this.state.activity === 'extreme'}
              onPress={() => this.setState({ activity: 'extreme' })}
            />
            <Body>
              <Text>
                {this.props.system.language === 'en'
                  ? 'Extreme\n(very heavy exercise or physical job)'
                  : 'Extrême\n(exercice très intense ou travail physique)'}
              </Text>
            </Body>
          </ListItem>
          <Button
            style={styles.submitButton}
            onPress={() => this.setActivity()}
          >
            <Text style={styles.submitButtonText}>
              {this.props.system.language === 'en' ? 'Next' : 'Suivant'}
            </Text>
          </Button>
        </Card>
      </ImageBackground>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskActivityRedux = connect(mapStateToProps, { updateActivity })(
  AskActivity,
);

export default AskActivityRedux;
