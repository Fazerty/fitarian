import React, { Component } from 'react';
import { StyleSheet, Modal, ImageBackground } from 'react-native';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateWeight } from '../../store/system/actions';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { WeightUnit, Sex } from '../../entities';
import {
  Card,
  CardItem,
  H1,
  Form,
  Item,
  Input,
  Icon,
  Text,
  Picker,
  Button,
  Grid,
  Col,
  Row,
} from 'native-base';

interface Props {
  updateWeight: typeof updateWeight;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class AskWeight extends Component<Props> {
  state = {
    value: this.props.system.weight
      ? this.props.system.weight.value
      : undefined,
    unit:
      this.props.system.weight && this.props.system.weight.unit
        ? this.props.system.weight.unit
        : 'kg',
    infoModal: false,
  };

  /**
   * G. J. Hamwi Formula (1964)
   * Male:	48.0 kg + 2.7 kg per inch over 5 feet
   * Female:	45.5 kg + 2.2 kg per inch over 5 feet
   *
   * Invented for medicinal dosage purposes.
   *
   * @param {Sex} sex
   * @param {number} heightInch
   * @returns {number} the weight in kg
   * @memberof AskWeight
   */
  hamwiFormula(sex: Sex, heightInch: number): number {
    if (sex === 'm') {
      return 48 + 2.7 * (heightInch - 5 * 12);
    } else {
      return 45.5 + 2.2 * (heightInch - 5 * 12);
    }
  }

  /**
   * B. J. Devine Formula (1974)
   * Male:	50.0 kg + 2.3 kg per inch over 5 feet
   * Female:	45.5 kg + 2.3 kg per inch over 5 feet
   *
   * Similar to the Hamwi Formula, it was originally intended as a basis for medicinal dosages based on weight and height. Over time, the formula became a universal determinant of IBW.
   *
   * @param {Sex} sex
   * @param {number} heightInch
   * @returns {number} the weight in kg
   * @memberof AskWeight
   */
  devineFormula(sex: Sex, heightInch: number): number {
    if (sex === 'm') {
      return 50 + 2.3 * (heightInch - 5 * 12);
    } else {
      return 45.5 + 2.3 * (heightInch - 5 * 12);
    }
  }

  /**
   * J. D. Robinson Formula (1983)
   * Male:	52 kg + 1.9 kg per inch over 5 feet
   * Female:	49 kg + 1.7 kg per inch over 5 feet
   *
   * Modification of the Devine Formula.
   *
   * @param {Sex} sex
   * @param {number} heightInch
   * @returns {number} the weight in kg
   * @memberof AskWeight
   */
  robinsonFormula(sex: Sex, heightInch: number): number {
    if (sex === 'm') {
      return 52 + 1.9 * (heightInch - 5 * 12);
    } else {
      return 49 + 1.7 * (heightInch - 5 * 12);
    }
  }

  /**
   * D. R. Miller Formula (1983)
   * Male:	56.2 kg + 1.41 kg per inch over 5 feet
   * Female:	53.1 kg + 1.36 kg per inch over 5 feet
   *
   * @param {Sex} sex
   * @param {number} heightInch
   * @returns {number} the weight in kg
   * @memberof AskWeight
   */
  millerFormula(sex: Sex, heightInch: number): number {
    if (sex === 'm') {
      return 56.2 + 1.41 * (heightInch - 5 * 12);
    } else {
      return 53.1 + 1.36 * (heightInch - 5 * 12);
    }
  }

  private formulaAndNames = [
    { name: 'G. J. Hamwi (1964)', formula: this.hamwiFormula },
    { name: 'B. J. Devine (1974)', formula: this.devineFormula },
    { name: 'J. D. Robinson (1983)', formula: this.robinsonFormula },
    { name: 'D. R. Miller (1983)', formula: this.millerFormula },
  ];

  /**
   * Gets a list of ideal weights (in the choosen unit) and the name of the formula used to calculate them.
   *
   *
   * @memberof AskWeight
   */
  getIdealWeights(): { name: string; value: number }[] {
    const unit: WeightUnit = this.state.unit
      ? this.state.unit
      : this.props.system.weight && this.props.system.weight.unit
      ? this.props.system.weight.unit
      : 'kg';
    const sex = this.props.system.sex;
    const heightInch: number = this.props.system.height
      ? this.props.system.height.unit === 'inch'
        ? this.props.system.height.value
        : this.props.system.height.value / 2.54
      : 150;
    return this.formulaAndNames.map(
      ({
        name,
        formula,
      }: {
        name: string;
        formula: (sex: Sex, heightInch: number) => number;
      }) => {
        const weightKg: number = formula(sex, heightInch);
        return {
          name,
          value: Math.floor(unit === 'kg' ? weightKg : weightKg * 2.204623),
        };
      },
    );
  }

  /**
   * Handles the change of the value in the form
   *
   * @memberof AskName
   */
  handleValue = (text: string) => {
    this.setState({ value: text.replace(/[^0-9]/g, '') });
  };

  /**
   * Handles the change of the unit in the form
   *
   * @memberof AskName
   */
  handleUnit = (unit: WeightUnit) => {
    this.setState({ unit });
  };

  /**
   * Stores the weight and goes to the next question page.
   *
   * @memberof AskWeight
   */
  setWeight() {
    this.props.updateWeight({ value: this.state.value, unit: this.state.unit });
    this.props.navigation.navigate('Activity');
  }

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#CC9966',
        }}
        source={require('../../assets/theme-inn-iLKK0eFTywU-unsplash.jpg')}
        resizeMode="cover"
      >
        <Modal
          visible={this.state.infoModal}
          animationType="slide"
          onRequestClose={() => {
            this.setState({ infoModal: false });
          }}
        >
          <Card
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          >
            <CardItem>
              <H1>
                {this.props.system.language === 'en'
                  ? 'Your ideal weight'
                  : 'Votre poids idéal'}
              </H1>
            </CardItem>

            <CardItem>
              <Grid
                style={{
                  borderColor: 'black',
                  borderStyle: 'solid',
                  borderWidth: 1,
                }}
              >
                <Row
                  style={{
                    backgroundColor: '#ccffe6',
                    borderColor: 'black',
                    height: 40,
                    alignItems: 'center',
                  }}
                >
                  <Col>
                    <Text>Formula</Text>
                  </Col>
                  <Col style={{ alignItems: 'center' }}>
                    <Text>Ideal Weight</Text>
                  </Col>
                </Row>

                {this.getIdealWeights().map(
                  ({ name, value }: { name: string; value: number }) => {
                    return (
                      <Row
                        style={{
                          backgroundColor: '#00CE9F',
                          height: 40,
                          paddingTop: 10,
                        }}
                        key={name}
                      >
                        <Col>
                          <Text>{name}</Text>
                        </Col>

                        <Col style={{ alignItems: 'center' }}>
                          <Button
                            style={{ height: 20 }}
                            transparent
                            onPress={() => {
                              this.setState({
                                value: value.toString(),
                                infoModal: false,
                              });
                            }}
                          >
                            <Text>{value + ' ' + this.state.unit} </Text>
                          </Button>
                        </Col>
                      </Row>
                    );
                  },
                )}
              </Grid>
            </CardItem>

            <Button
              onPress={() => {
                this.setState({ infoModal: false });
              }}
            >
              <Text>
                {this.props.system.language === 'en' ? 'Close' : 'Fermer'}
              </Text>
            </Button>
          </Card>
        </Modal>
        <Card style={{ justifyContent: 'center' }}>
          <CardItem>
            <H1>
              {this.props.system.language === 'en'
                ? 'The weight you want to reach'
                : 'Le poids que tu veux atteindre'}
            </H1>
          </CardItem>
          <CardItem>
            <Text>
              {this.props.system.language === 'en'
                ? 'Consult your ideal weight'
                : 'Consulte ton poids idéal'}
            </Text>
            <Button
              iconLeft
              transparent
              onPress={() => {
                this.setState({ infoModal: true });
              }}
            >
              <Icon type="FontAwesome" name="question"></Icon>
            </Button>
          </CardItem>
          <Form>
            <Item>
              <Input
                underlineColorAndroid="transparent"
                placeholder={
                  this.props.system.language === 'en'
                    ? ' ...your weight'
                    : ' ...ton poids'
                }
                placeholderTextColor="#9a73ef"
                keyboardType="numeric"
                autoCapitalize="none"
                onChangeText={this.handleValue}
                value={this.state.value ? this.state.value + '' : ''}
              />
            </Item>
            <Item>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                placeholder={
                  this.props.system.language === 'en'
                    ? ' Select a unit'
                    : ' Sélectionne une unité'
                }
                placeholderStyle={{ color: '#bfc6ea' }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.unit}
                onValueChange={this.handleUnit.bind(this)}
              >
                <Picker.Item label="kg" value="kg" />
                <Picker.Item label="lbs" value="lbs" />
              </Picker>
            </Item>
          </Form>
          <Button
            disabled={!this.state.value && !this.state.unit}
            style={styles.submitButton}
            onPress={() => this.setWeight()}
          >
            <Text style={styles.submitButtonText}>
              {this.props.system.language === 'en' ? 'Next' : 'Suivant'}
            </Text>
          </Button>
        </Card>
      </ImageBackground>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskWeightRedux = connect(mapStateToProps, { updateWeight })(AskWeight);

export default AskWeightRedux;
