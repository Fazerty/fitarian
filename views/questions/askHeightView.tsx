import React, { Component } from 'react';
import { Text, View, StyleSheet, ImageBackground } from 'react-native';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateHeight } from '../../store/system/actions';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { HeightUnit } from '../../entities';

import {
  Form,
  Item,
  Input,
  Picker,
  Icon,
  Card,
  CardItem,
  H1,
  Button,
} from 'native-base';

interface Props {
  updateHeight: typeof updateHeight;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class AskHeight extends Component<Props> {
  state = {
    value: this.props.system.height
      ? this.props.system.height.value
      : undefined,
    unit:
      this.props.system.height && this.props.system.height.unit
        ? this.props.system.height.unit
        : 'cm',
  };

  /**
   * Handles the change of the value in the form
   *
   * @memberof AskName
   */
  handleValue = (text: string) => {
    this.setState({ value: text.replace(/[^0-9]/g, '') });
  };

  /**
   * Handles the change of the unit in the form
   *
   * @memberof AskName
   */
  handleUnit = (unit: HeightUnit) => {
    this.setState({ unit });
  };

  /**
   * Stores the height and goes to the next question page.
   *
   * @memberof AskHeight
   */
  setHeight() {
    this.props.updateHeight({ value: this.state.value, unit: this.state.unit });
    this.props.navigation.navigate('Weight');
  }

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#CC9966',
        }}
        source={require('../../assets/viktor-ritsvall-i_UjgJDNgdw-unsplash.jpg')}
        resizeMode="stretch"
      >
        <Card
          style={{ flex: 1, alignItems: 'stretch', alignContent: 'stretch', width: 200 }}
        >
          <CardItem style={{ paddingTop: 60, paddingBottom: 100 }}>
            <H1>
              {this.props.system.language === 'en'
                ? 'Your height'
                : 'Ta taille'}
            </H1>
          </CardItem>
          <CardItem style={{ flex: 1, alignItems: 'center', alignContent: 'center' }}>
            <Form style={{ flex: 1, alignItems: 'center', alignContent: 'center' }}>
              <Item>
                <Input
                  underlineColorAndroid="transparent"
                  placeholder={
                    this.props.system.language === 'en'
                      ? ' ...your height'
                      : ' ...ta taille'
                  }
                  placeholderTextColor="#9a73ef"
                  keyboardType="numeric"
                  autoCapitalize="none"
                  onChangeText={this.handleValue}
                  value={this.state.value ? this.state.value + '' : ''}
                />
              </Item>
              <Item>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  placeholder={
                    this.props.system.language === 'en'
                      ? 'Select a unit'
                      : 'Sélectionne une unité'
                  }
                  placeholderStyle={{ color: '#bfc6ea' }}
                  placeholderIconColor="#007aff"
                  selectedValue={this.state.unit}
                  onValueChange={this.handleUnit.bind(this)}
                >
                  <Picker.Item label="cm" value="cm" />
                  <Picker.Item
                    label={
                      this.props.system.language === 'en' ? 'inch' : 'pouce'
                    }
                    value="inch"
                  />
                </Picker>
              </Item>
            </Form>
          </CardItem>
          <CardItem>
            <Button
              disabled={!this.state.value && !this.state.unit}
              style={styles.submitButton}
              onPress={() => this.setHeight()}
            >
              <Text style={styles.submitButtonText}>
                {this.props.system.language === 'en' ? 'Next' : 'Suivant'}
              </Text>
            </Button>
          </CardItem>
        </Card>
      </ImageBackground>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskHeightRedux = connect(mapStateToProps, { updateHeight })(AskHeight);

export default AskHeightRedux;
