import React, { Component } from 'react';
import { SystemState, ActivityType } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateFormula } from '../../store/system/actions';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import {
  Body,
  Text,
  CheckBox,
  Card,
  CardItem,
  H1,
  H2,
  Button,
  View,
  ListItem,
  List,
} from 'native-base';
import { Sex } from '../../entities';
import { getAge } from './askDateOfBirthView';
import { StyleSheet } from 'react-native';

interface Props {
  updateFormula: typeof updateFormula;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class AskFormula extends Component<Props> {
  state: { formula: string } = {
    formula: this.props.system.formula
      ? this.props.system.formula
      : 'Harris-Benedict',
  };

  /**
   * Stores the formula name to calculate the daily metabolic rate and goes to the next question page.
   *
   * @memberof AskFormula
   */
  setFormula() {
    const formula: string = this.state.formula;
    this.props.updateFormula(formula);
    this.props.navigation.navigate('Calculator', { screen: 'MenuList' });
  }

  /**
   * BMR stands for Basal Metabolic Rate and represents your daily energy expenditure if your body is at rest all day.
   * Look upon it as the minimum number of calories your body needs in order to function for 24 hours.
   *
   * Harris-Benedict formula To calculate the BMR
   *
   * Metric formula for men
   * BMR = 66.47 + ( 13.75 × weight in kg ) + ( 5.003 × height in cm ) − ( 6.755 × age in years )
   *
   * Metric formula for women
   * BMR = 65.1 + ( 9.563 × weight in kg ) + ( 1.85 × height in cm ) − ( 4.676 × age in years )
   *
   * @memberof AskFormula
   */
  harrisBenedictEquation(
    sex: Sex,
    heightCm: number,
    age: number,
    weightKg: number,
  ): number {
    if (sex === 'm') {
      return 66.47 + 13.75 * weightKg + 5.003 * heightCm - 6.755 * age;
    } else {
      return 655.1 + 9.563 * weightKg + 1.85 * heightCm - 4.676 * age;
    }
  }
  /**
   * BMR stands for Basal Metabolic Rate and represents your daily energy expenditure if your body is at rest all day.
   * Look upon it as the minimum number of calories your body needs in order to function for 24 hours.
   *
   * Mifflin - St Jeor formula To calculate the BMR
   *
   * Metric formula for men   *
   * BMR = (10 × weight in kg) + (6.25 × height in cm) − (5 × age in years) + 5
   *
   * Metric formula for women
   * BMR = (10 × weight in kg) + (6.25 × height in cm) − (5 × age in years) − 161
   *
   * @memberof AskFormula
   */
  mifflinStJeorEquation(
    sex: Sex,
    heightCm: number,
    age: number,
    weightKg: number,
  ): number {
    if (sex === 'm') {
      return 10 * weightKg + 6.25 * heightCm - 5 * age + 5;
    } else {
      return 10 * weightKg + 6.25 * heightCm - 5 * age - 161;
    }
  }

  /**
   * If you are sedentary (little or no exercise)
   * Calories Per Day = BMR x 1.2
   * If you are lightly active (light exercise or sports 1-3 days/week)
   * Calories Per Day = BMR x 1.375
   * If you are moderately active (moderate exercise 3-5 days/week)
   * Calories Per Day = BMR x 1.55
   * If you are very active (hard exercise 6-7 days/week)
   * Calories Per Day = BMR x 1.725
   * If you are super active (very hard exercise and a physical job)
   * Calories Per Day = BMR x 1.9
   *
   * @param {number} brm
   * @returns {number}
   * @memberof AskFormula
   */
  brmToDaily(brm: number, activity: ActivityType): number {
    switch (activity) {
      case 'sedentary':
        return brm * 1.2;
      case 'light':
        return brm * 1.375;
      case 'moderate':
        return brm * 1.55;
      case 'high':
        return brm * 1.725;
      case 'extreme':
        return brm * 1.9;
    }
  }

  /**
   * The brm corrected by the activity
   * TODO: make it static and public
   * @returns {number} in kilocalories
   * @memberof AskFormula
   */
  getDailyMetabolicRate(): number {
    if (this.props.system.weight && this.props.system.height) {
      const weightKg: number =
        this.props.system.weight.unit === 'kg'
          ? this.props.system.weight.value
          : this.props.system.weight.value * 2.204623;
      const sex = this.props.system.sex;
      const age: number = getAge(this.props.system.dob);
      const activity: ActivityType = this.props.system.activity;
      const heightCm: number =
        this.props.system.height.unit === 'cm'
          ? this.props.system.height.value
          : this.props.system.height.value * 2.54;

      return Math.floor(
        this.brmToDaily(
          this.equationByName.get(this.state.formula)(
            sex,
            heightCm,
            age,
            weightKg,
          ),
          activity,
        ),
      );
    }
    return 0;
  }

  private equationByName: Map<string, any> = new Map<string, any>();

  render() {
    this.equationByName.set('Harris-Benedict', this.harrisBenedictEquation);
    this.equationByName.set('Mifflin - St Jeor', this.mifflinStJeorEquation);

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Card>
          <CardItem>
            <H1>
              {this.props.system.language === 'en'
                ? 'Your metabolic rate,'
                : 'Ton métabolisme journalier,'}
            </H1>
          </CardItem>
          <CardItem>
            <H2>
              {this.props.system.language === 'en'
                ? 'for your expected weight,'
                : 'pour ton poids attendu,'}
            </H2>
          </CardItem>
          <CardItem>
            <H2>
              {this.props.system.language === 'en' ? 'with the ' : 'avec '}
            </H2>
          </CardItem>
          <List>
            <ListItem>
              <CheckBox
                checked={this.state.formula === 'Harris-Benedict'}
                onPress={() => this.setState({ formula: 'Harris-Benedict' })}
              />
              <Body>
                <Text>
                  {this.props.system.language === 'en'
                    ? 'Harris-Benedict equation'
                    : "l'équation de Harris-Benedict"}
                </Text>
              </Body>
            </ListItem>
            <ListItem>
              <CheckBox
                checked={this.state.formula === 'Mifflin - St Jeor'}
                onPress={() => this.setState({ formula: 'Mifflin - St Jeor' })}
              />
              <Body>
                <Text>
                  {this.props.system.language === 'en'
                    ? 'Mifflin - St Jeor Equation'
                    : 'Equation de Mifflin - St Jeor '}
                </Text>
              </Body>
            </ListItem>
          </List>
          <CardItem>
            <H2>is {this.getDailyMetabolicRate()} kilocalories </H2>
          </CardItem>
          <Button style={styles.submitButton} onPress={() => this.setFormula()}>
            <Text style={styles.submitButtonText}>
              {this.props.system.language === 'en' ? 'Next' : 'Suivant'}
            </Text>
          </Button>
        </Card>
      </View>
    );
  }
}
// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});
// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskFormulaRedux = connect(mapStateToProps, { updateFormula })(AskFormula);

export default AskFormulaRedux;
