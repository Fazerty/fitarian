import React, { Component } from 'react';
import { Text, View, StyleSheet, ImageBackground } from 'react-native';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateName } from '../../store/system/actions';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { Card, CardItem, H1, H2, Button, Input, Form, Item } from 'native-base';

interface Props {
  updateName: typeof updateName;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class AskName extends Component<Props> {
  state = {
    name: this.props.system.userName,
  };

  /**
   * Stores the name and goes to the next question page.
   *
   * @param {string} name
   * @memberof AskName
   */
  setName(name: string) {
    this.props.updateName(name);
    this.props.navigation.navigate('Sex');
  }

  /**
   * Handles the change of the name in the input
   *
   * @memberof AskName
   */
  handleName = (text: string) => {
    this.setState({ name: text });
  };

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        source={require('../../assets/taylor-simpson-Gy7fiskX-Sk-unsplash.jpg')}
        resizeMode="stretch"
      >
        <View style={styles.container}>
          <Card>
            <CardItem>
              <H1>
                {this.props.system.language === 'en'
                  ? "I'm Fitarian"
                  : 'Je suis Fitarian'}
              </H1>
            </CardItem>
            <CardItem>
              <H2>
                {this.props.system.language === 'en'
                  ? 'How would you like me to call you?'
                  : 'Comment aimeriez-vous que je vous appelle?'}
              </H2>
            </CardItem>
            <Form>
              <Item>
                <Input
                  underlineColorAndroid="transparent"
                  placeholder={
                    this.props.system.language === 'en'
                      ? ' ...your name'
                      : ' ...votre nom'
                  }
                  placeholderTextColor="#9a73ef"
                  autoCapitalize="none"
                  onChangeText={this.handleName}
                  value={this.state.name ? this.state.name : ''}
                />
              </Item>
            </Form>
            <Button
              disabled={!this.state.name}
              style={styles.submitButton}
              onPress={() => this.setName(this.state.name)}
            >
              <Text style={styles.submitButtonText}> Next </Text>
            </Button>
          </Card>
        </View>
      </ImageBackground>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskNameRedux = connect(mapStateToProps, { updateName })(AskName);

export default AskNameRedux;
