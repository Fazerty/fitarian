import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground } from 'react-native';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateDob } from '../../store/system/actions';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { Card, CardItem, H1, H2, Button } from 'native-base';
import DateTimePicker from '@react-native-community/datetimepicker';

interface Props {
  updateDob: typeof updateDob;
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export function getAge(dob: Date) {
  const currDate = new Date();
  var age = currDate.getFullYear() - dob.getFullYear();
  var m = currDate.getMonth() - dob.getMonth();
  if (m < 0 || (m === 0 && currDate.getDate() < dob.getDate())) {
    age--;
  }
  return age;
}

export class AskDateOfBirth extends Component<Props> {
  state = {
    dob: this.props.system.dob,
    show: false,
  };

  /**
   * Stores the date of birth and goes to the next question page.
   *
   * @memberof AskDateOfBirth
   */
  setDob(dob: Date) {
    this.props.updateDob(dob);
    this.props.navigation.navigate('Height');
  }

  /**
   * Handles the change of the dob
   *
   * @memberof AskDateOfBirth
   */
  handleDob = (dob: Date) => {
    this.setState({ dob, show: false });
  };

  showDatepicker = () => {
    this.setState({ show: true });
  };

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        source={require('../../assets/jason-leung-Xaanw0s0pMk-unsplash.jpg')}
        resizeMode="stretch"
      >
        <Card>
          <CardItem>
            <H2>
              {this.props.system.language === 'en'
                ? 'I need to know your date of birth'
                : "J'ai besoin de connaître ta date de naissance"}
            </H2>
          </CardItem>
          <CardItem>
            <H2>
              {this.props.system.language === 'en'
                ? 'To calculate your age at any time'
                : 'Pour calculer ton age à tout moment.'}
            </H2>
          </CardItem>

          <CardItem>
            <Button
              onPress={this.showDatepicker}
              style={{ flex: 1, alignContent: 'center' }}
            >
              <Text
                style={{
                  flex: 1,
                  fontSize: 20,
                  color: 'white',
                  textAlign: 'center',
                }}
              >
                {this.state.dob
                  ? this.state.dob.toLocaleDateString()
                  : this.props.system.language === 'en'
                  ? 'Choose a date'
                  : 'Choisir une date'}
              </Text>
            </Button>
            {this.state.show && (
              <DateTimePicker
                value={this.props.system.dob ? this.props.system.dob : new Date() }
                minimumDate={new Date(1900, 1, 1)}
                maximumDate={new Date()}
                mode="date"
                locale={this.props.system.language}
                timeZoneOffsetInMinutes={undefined}
                onChange={(_event: any, date?: Date) => this.handleDob(date)}
              />
            )}
          </CardItem>
        </Card>
        <Button
          disabled={!this.state.dob}
          style={styles.submitButton}
          onPress={() => this.setDob(this.state.dob)}
        >
          <Text style={styles.submitButtonText}> Next </Text>
        </Button>
      </ImageBackground>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskDobRedux = connect(mapStateToProps, { updateDob })(AskDateOfBirth);

export default AskDobRedux;
