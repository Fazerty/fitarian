import React, { Component } from 'react';
import {
  Text,
  View,
  Button,
  Icon,
  List,
  ListItem,
  H1,
  H2,
  Spinner,
  Card,
  CardItem,
  Body,
  Grid,
  Col,
  Input,
  H3,
  Picker,
  Form,
  Item,
  Row,
} from 'native-base';
import { SystemState, ActivityType } from '../store/system/types';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { MenuService } from '../services/menuService';
import { Menu, Ingredient, Alim, Sex } from '../entities';
import { Modal } from 'react-native';
import AlimSearch from '../components/alim/alimSearch.component';
import { AlimService } from '../services/alimService';
import { AppState } from '../store/store';
import { connect } from 'react-redux';
import { getAge } from './questions/askDateOfBirthView';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { updateMenu, deleteMenu } from '../store/system/actions';
import { IngredientService } from '../services/ingredientService';

interface MenuProps {
  system: SystemState;
  id: number | undefined;
  updateMenu: typeof updateMenu;
  deleteMenu: typeof deleteMenu;
  route: any;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class MenuEdit extends Component<MenuProps> {
  state: {
    isLoading: boolean;
    ingredients: Ingredient[];
    alimPickerModal: boolean; // Set the open state of the modal to select an alim
    changeQuantityModal: boolean; // Set the open state of the modal to edit the quantity of ingredients
    ingredientEdited: Ingredient; // the ingredient edited in the changeQuantityModal
    ingredientEditedValue: number;
    ingredientEditedUnit: 'g' | 'oz';
    title: string;
  } = {
    isLoading: true,
    ingredients: [],
    alimPickerModal: false,
    changeQuantityModal: false,
    ingredientEdited: undefined,
    ingredientEditedValue: 0,
    ingredientEditedUnit: 'g',
    title: '',
  };

  private alimService: AlimService = new AlimService();
  private menuService: MenuService = new MenuService();
  private ingredientService: IngredientService = new IngredientService();
  private menu: Menu;

  async selectAlim(newAlim: Alim) {
    if (newAlim) {
      const alim: Alim = await this.alimService.saveAlim(newAlim);
      const ingredient: Ingredient = new Ingredient(alim);
      // ??? TO FIX
      ingredient.alim = alim;
      ingredient.quantityValue = 0;
      ingredient.quantityUnit = 'g';

      this.setState({
        ingredients: [...this.state.ingredients, ingredient],
        alimPickerModal: false,
      });
      this.menu.ingredients = this.state.ingredients;
      this.menuService.updateMenu(this.menu);
    }
  }

  constructor(props: Readonly<MenuProps>) {
    super(props);
    this.selectAlim = this.selectAlim.bind(this);
  }

  addIngredient() {
    this.setState({ alimPickerModal: true });
  }

  changeIngredientEdited() {
    this.state.ingredientEdited.quantityValue = this.state.ingredientEditedValue;
    this.state.ingredientEdited.quantityUnit = this.state.ingredientEditedUnit;
    this.ingredientService.updateIngredient(this.state.ingredientEdited);
    this.setState({ changeQuantityModal: false });
  }

  /**
   * Changes the title of the menu. Stores the change in the db.
   *
   * @param {string} newTitle
   * @memberof MenuEdit
   */
  async changeTitle(newTitle: string) {
    this.menu.title = newTitle;
    // await // Too slow
    this.props.updateMenu(this.menu);
    this.setState({ title: newTitle });
  }

  componentDidMount() {
    const id: number | undefined = this.props.route
      ? this.props.route.params
        ? this.props.route.params.id
        : undefined
      : undefined;
    this.menuService
      .getMenu(id, this.props.system.language)
      .then((menu: Menu) => {
        this.menu = menu;
        if (!id) {
          this.props.updateMenu(this.menu, true);
        }
        this.setState({
          ingredients: this.menu.ingredients,
          title: this.menu.title,
        });
        this.setState({ isLoading: false });
      });
  }

  /**
   * BMR stands for Basal Metabolic Rate and represents your daily energy expenditure if your body is at rest all day.
   * Look upon it as the minimum number of calories your body needs in order to function for 24 hours.
   *
   * Harris-Benedict formula To calculate the BMR
   *
   * Metric formula for men
   * BMR = 66.47 + ( 13.75 × weight in kg ) + ( 5.003 × height in cm ) − ( 6.755 × age in years )
   *
   * Metric formula for women
   * BMR = 65.1 + ( 9.563 × weight in kg ) + ( 1.85 × height in cm ) − ( 4.676 × age in years )
   *
   * @memberof AskFormula
   */
  harrisBenedictEquation(
    sex: Sex,
    heightCm: number,
    age: number,
    weightKg: number,
  ): number {
    if (sex === 'm') {
      return 66.47 + 13.75 * weightKg + 5.003 * heightCm - 6.755 * age;
    } else {
      return 655.1 + 9.563 * weightKg + 1.85 * heightCm - 4.676 * age;
    }
  }
  /**
   * BMR stands for Basal Metabolic Rate and represents your daily energy expenditure if your body is at rest all day.
   * Look upon it as the minimum number of calories your body needs in order to function for 24 hours.
   *
   * Mifflin - St Jeor formula To calculate the BMR
   *
   * Metric formula for men   *
   * BMR = (10 × weight in kg) + (6.25 × height in cm) − (5 × age in years) + 5
   *
   * Metric formula for women
   * BMR = (10 × weight in kg) + (6.25 × height in cm) − (5 × age in years) − 161
   *
   * @memberof AskFormula
   */
  mifflinStJeorEquation(
    sex: Sex,
    heightCm: number,
    age: number,
    weightKg: number,
  ): number {
    if (sex === 'm') {
      return 10 * weightKg + 6.25 * heightCm - 5 * age + 5;
    } else {
      return 10 * weightKg + 6.25 * heightCm - 5 * age - 161;
    }
  }

  /**
   * If you are sedentary (little or no exercise)
   * Calories Per Day = BMR x 1.2
   * If you are lightly active (light exercise or sports 1-3 days/week)
   * Calories Per Day = BMR x 1.375
   * If you are moderately active (moderate exercise 3-5 days/week)
   * Calories Per Day = BMR x 1.55
   * If you are very active (hard exercise 6-7 days/week)
   * Calories Per Day = BMR x 1.725
   * If you are super active (very hard exercise and a physical job)
   * Calories Per Day = BMR x 1.9
   *
   * @param {number} brm
   * @returns {number}
   * @memberof AskFormula
   */
  brmToDaily(brm: number, activity: ActivityType): number {
    switch (activity) {
      case 'sedentary':
        return brm * 1.2;
      case 'light':
        return brm * 1.375;
      case 'moderate':
        return brm * 1.55;
      case 'high':
        return brm * 1.725;
      case 'extreme':
        return brm * 1.9;
    }
  }

  /**
   * The brm corrected by the activity
   * TODO: make it static and public
   * @returns {number} in kilocalories
   * @memberof AskFormula
   */
  getDailyMetabolicRate(): number {
    const weightKg: number =
      this.props.system.weight.unit === 'kg'
        ? this.props.system.weight.value
        : this.props.system.weight.value * 2.204623;
    const sex = this.props.system.sex;
    const age: number = getAge(this.props.system.dob);
    const activity: ActivityType = this.props.system.activity;
    const heightCm: number =
      this.props.system.height.unit === 'cm'
        ? this.props.system.height.value
        : this.props.system.height.value * 2.54;

    return Math.floor(
      this.brmToDaily(
        this.equationByName.get(this.props.system.formula)(
          sex,
          heightCm,
          age,
          weightKg,
        ),
        activity,
      ),
    );
  }

  /**
   * Returns the sum of the ingredient Kcals.
   *
   * @param {Ingredient} [ingredientToSkip] Used when the change quantity modal is opened. It's to have the sum of Kcals without the kcal of the ingredient edited.
   * @returns {number}
   * @memberof MenuEdit
   */
  getKcalIntake(ingredientToSkip?: Ingredient): number {
    return this.state.ingredients
      .map((ingredient: Ingredient) => {
        if (ingredientToSkip && ingredient.id === ingredientToSkip.id) {
          return 0;
        } else {
          const kcal = ingredient.getKCal();
          return kcal ? kcal : 0;
        }
      })
      .reduce((previousKcal: number, currentKcal: number) => {
        return previousKcal + currentKcal;
      }, 0);
  }

  private equationByName: Map<string, any> = new Map<string, any>();

  /**
   * Extracts the key for the ingredient list
   *
   * @memberof MenuEdit
   */
  _keyExtractor = (_item, index) => index.toString();

  editIngredientQuantity = (ingredient: Ingredient) => {
    this.setState({
      changeQuantityModal: true,
      ingredientEdited: ingredient,
      ingredientEditedUnit: ingredient.quantityUnit,
      ingredientEditedValue: ingredient.quantityValue,
    });
  };

  async deleteIngredient(ingredient: Ingredient) {
    this.menu.ingredients = this.state.ingredients;
    this.menu.ingredients.splice(
      this.state.ingredients
        .map(function (x) {
          return x.id;
        })
        .indexOf(ingredient.id),
      1,
    );
    // await this.props.updateMenu(this.menu);
    this.menuService.updateMenu(this.menu);
    this.setState({
      ingredients: [...this.menu.ingredients],
    });
  }

  getIngredientKcalInfo() {
    const ingredientEditedKcal = this.state.ingredientEdited.getKCal(
      this.state.ingredientEditedValue,
      this.state.ingredientEditedUnit,
    );
    const otherIngredientsKCal = this.getKcalIntake(
      this.state.ingredientEdited,
    );
    const getDailyMetabolicRate = this.getDailyMetabolicRate();
    return (
      <Grid
        style={{
          alignContent: 'center',
          maxHeight: 130,
          paddingTop: 30,
          paddingBottom: 30,
          paddingLeft: 20,
          paddingRight: 20,
        }}
      >
        <Row>
          <Col size={15}>
            <Text
              style={{
                color: 'gray',
              }}
            >
              {ingredientEditedKcal}
            </Text>
          </Col>
          <Col size={25}>
            <Text> Kcal</Text>
          </Col>
          <Col size={60}>
            <Text>
              (total: {ingredientEditedKcal + otherIngredientsKCal} /{' '}
              {getDailyMetabolicRate} Kcal.)
            </Text>
          </Col>
        </Row>
        <Row>
          <Col size={20}>
            <Text
              style={{
                color: 'gray',
              }}
            >
              {Math.floor(
                (ingredientEditedKcal / getDailyMetabolicRate) * 1000,
              ) / 10}{' '}
              %
            </Text>
          </Col>
          <Col size={80}>
            <Text>
              {this.props.system.language === 'en'
                ? 'of the daily intake'
                : 'de la consommation journalière'}
            </Text>
          </Col>
        </Row>
      </Grid>
    );
  }

  /**
   * Renders an ingredient item.
   *
   * @memberof MenuEdit
   */
  _renderIngredientItem = ({ item }) => (
    <TouchableOpacity>
      <Card style={{ flex: 1 }}>
        <CardItem>
          <Body>
            <Grid>
              <Col size={85}>
                <Text>
                  {this.props.system.language === 'en'
                    ? item.alim.alim_nom_eng
                    : item.alim.alim_nom_fr}
                </Text>
              </Col>
              <Col size={15}>
                <Button
                  block
                  danger
                  small
                  bordered
                  onPress={() => this.deleteIngredient(item)}
                >
                  <Icon type="FontAwesome" name="trash"></Icon>
                </Button>
              </Col>
            </Grid>

            <Grid>
              <Col size={70}>
                <Button
                  icon
                  small
                  transparent
                  iconRight
                  onPress={() => this.editIngredientQuantity(item)}
                >
                  <Text style={{ textAlign: 'center' }}>
                    {item.quantityValue} {item.quantityUnit}
                  </Text>
                  <Icon fontSize={10} type="FontAwesome" name="pencil"></Icon>
                </Button>
              </Col>
              <Col size={30}>
                <Text
                  style={{ textAlign: 'right', paddingTop: 5, color: 'gray' }}
                >
                  {item.getKCal() ?? '?'} Kcal
                </Text>
              </Col>
            </Grid>
          </Body>
        </CardItem>
      </Card>
    </TouchableOpacity>
  );

  /**
   * Handles the change of the unit of an ingredient in the form
   *
   * @memberof MenuEdit
   */
  handleIngredientUnit = (unit: 'g' | 'oz') => {
    this.setState({ ingredientEditedUnit: unit });
  };

  /**
   * Handles the change of the value of an ingredient in the form
   *
   * @memberof MenuEdit
   */
  handleIngredientValue = (value: string) => {
    const parsedValue = parseInt(value.replace(/[^0-9]/g, ''));
    this.setState({
      ingredientEditedValue: Number.isNaN(parsedValue) ? 0 : parsedValue
    });
  };

  render() {
    this.equationByName.set('Harris-Benedict', this.harrisBenedictEquation);
    this.equationByName.set('Mifflin - St Jeor', this.mifflinStJeorEquation);

    if (this.state.isLoading) {
      return (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
        >
          <Spinner />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Input
            style={{
              flex: 1,
              minHeight: 65,
              maxHeight: 65,
              fontSize: 25,
              textAlign: 'center',
              paddingTop: 15,
              paddingBottom: 15,
            }}
            placeholder={
              this.props.system.language === 'en' ? 'Title' : 'Titre'
            }
            value={this.state.title}
            onChangeText={(text) => this.changeTitle(text)}
          />
          <H2 style={{ padding: 10, backgroundColor: '#DCDCDC' }}>
            {this.props.system.language === 'en'
              ? 'Ingredients'
              : 'Ingrédients'}
          </H2>
          <FlatList style={{minHeight: 480, maxHeight: 480}}
            data={this.state.ingredients}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderIngredientItem}
          />
          <Grid
            style={{
              alignItems: 'flex-end',
              maxHeight: 70,
            }}
          >
            <Row
              style={{
                alignItems: 'center',
              }}
            >
              <Col size={50}></Col>
              <Col size={15}>
                <Text style={{ paddingLeft: 7 }}>
                  {this.props.system.language === 'en' ? 'Total: ' : 'Total: '}
                </Text>
              </Col>

              <Col size={10}>
                <Text style={{ color: 'gray' }}>{this.getKcalIntake()} </Text>
              </Col>
              <Col size={25}>
                <Text>
                  {this.props.system.language === 'en' ? '/' : '/'}{' '}
                  {this.getDailyMetabolicRate()} Kcal
                </Text>
              </Col>
            </Row>
            <Row
              style={{
                alignItems: 'center',
              }}
            >
              <Col size={50}></Col>
              <Col size={50}>
                <Button
                  small
                  style={{ justifyContent: 'center' }}
                  rounded
                  icon
                  iconRight
                  onPress={() => this.addIngredient()}
                >
                  <Text>
                    {this.props.system.language === 'en' ? 'Add' : 'Ajouter'}
                  </Text>
                  <Icon type="FontAwesome" name="plus" />
                </Button>
              </Col>
            </Row>
          </Grid>

          {/*
            The modal to edit the quantity value and unit of an ingredient
          */}
          <Modal
            visible={this.state.changeQuantityModal}
            animationType="slide"
            onRequestClose={() => {
              this.setState({ changeQuantityModal: false });
            }}
          >
            <View
              style={{
                backgroundColor: 'gray',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              {this.state.ingredientEdited && (
                <Card>
                  <CardItem>
                    <H2>
                      {this.props.system.language === 'en'
                        ? 'Change quantity of'
                        : 'Changer les quantités de'}
                    </H2>
                  </CardItem>
                  <CardItem>
                    <H3>
                      {this.props.system.language === 'en'
                        ? this.state.ingredientEdited.alim.alim_nom_eng
                        : this.state.ingredientEdited.alim.alim_nom_fr}
                    </H3>
                  </CardItem>
                  <CardItem>
                    <Form
                      style={{
                        flex: 1,
                      }}
                    >
                      <Item>
                        <Input
                          placeholder={
                            this.props.system.language === 'en'
                              ? 'enter a value'
                              : 'entre une valeur'
                          }
                          keyboardType="numeric"
                          placeholderTextColor="#9a73ef"
                          value={this.state.ingredientEditedValue ? this.state.ingredientEditedValue.toString() : ''}
                          onChangeText={this.handleIngredientValue}
                        />
                      </Item>
                      <Item>
                        <Picker
                          mode="dropdown"
                          iosIcon={<Icon name="arrow-down" />}
                          placeholder={
                            this.props.system.language === 'en'
                              ? 'Select a unit'
                              : 'Sélectionne une unité'
                          }
                          placeholderStyle={{ color: '#bfc6ea' }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.ingredientEditedUnit}
                          onValueChange={this.handleIngredientUnit.bind(this)}
                        >
                          <Picker.Item label="g" value="g" />
                          <Picker.Item
                            label={
                              this.props.system.language === 'en'
                                ? 'ounce'
                                : 'once'
                            }
                            value="oz"
                          />
                        </Picker>
                      </Item>
                    </Form>
                  </CardItem>
                  {this.getIngredientKcalInfo()}
                  <CardItem>
                    <Grid>
                      <Col>
                        <Button style={{justifyContent:'center'}}
                          small hasText
                          onPress={() => this.changeIngredientEdited()}
                        >
                          <Text>
                            {this.props.system.language === 'en'
                              ? 'Save'
                              : 'Enregister'}
                          </Text>
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          small hasText style={{justifyContent:'center'}} warning
                          onPress={() =>
                            this.setState({ changeQuantityModal: false })
                          }
                        >
                          <Text>
                            {this.props.system.language === 'en'
                              ? 'Cancel'
                              : 'Annuler'}
                          </Text>
                        </Button>
                      </Col>
                    </Grid>
                  </CardItem>
                </Card>
              )}
            </View>
          </Modal>

          {/*
            The modal to select an alim to add.
          */}
          <Modal
            visible={this.state.alimPickerModal}
            animationType="slide"
            onRequestClose={() => {
              this.setState({ alimPickerModal: false });
            }}
          >
            <Grid style={{ minHeight: 80, maxHeight: 80 }}>
              <Row>
                <Col size={90}>
                  <H2
                    style={{
                      paddingTop: 30,
                      paddingLeft: 20,
                      paddingBottom: 20,
                    }}
                  >
                    {this.props.system.language === 'en'
                      ? 'Choose an alim'
                      : 'Choisis un aliment'}
                  </H2>
                </Col>
                <Col
                  size={20}
                  style={{ justifyContent: 'center', alignContent: 'center' }}
                >
                  <Button
                    style={{ justifyContent: 'center' }}
                    icon
                    transparent
                    rounded
                    onPress={() => {
                      this.setState({ alimPickerModal: false });
                    }}
                  >
                    <Icon type="FontAwesome" name="window-close" />
                  </Button>
                </Col>
              </Row>
            </Grid>
            <AlimSearch
              language={this.props.system.language}
              onSelect={this.selectAlim}
            />
          </Modal>
        </View>
      );
    }
  }
}

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const MenuRedux = connect(mapStateToProps, { updateMenu, deleteMenu })(
  MenuEdit,
);

export default MenuRedux;
