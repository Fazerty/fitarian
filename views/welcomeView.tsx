import React, { Component } from 'react';
import { Text, View, Button, H1, Card, CardItem } from 'native-base';
import { AppState } from '../store/store';
import { connect } from 'react-redux';
import { SystemState } from '../store/system/types';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { StyleSheet, Image } from 'react-native';

interface WelcomeProps {
  system: SystemState;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class Welcome extends Component<WelcomeProps> {
  render() {

    return (
      <View style={styles.container}>
        <Card style={{ flex: 1 }}>
          <CardItem>
            <H1>{this.props.system.language === 'en' ? 'Hello' : 'Bonjour'}</H1>
          </CardItem>
          <CardItem>
            <Text>
              {this.props.system.language === 'en'
                ? 'To compute your daily metabolic rate,'
                : 'Pour calculer votre métabolisme journalier,'}
            </Text>

            {/* daily metabolic rate
            https://en.wikipedia.org/wiki/Basal_metabolic_rate */}
            {/* https://fr.wikipedia.org/wiki/M%C3%A9tabolisme_de_base */}
          </CardItem>
          <CardItem>
            <Image
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                height: 350,
              }}
              source={require('../assets/renee-fisher-lTkF2Tdx9uI-unsplash.jpg')}
              resizeMode="stretch"
            ></Image>
          </CardItem>
          <CardItem>
            <Text>
              {this.props.system.language === 'en'
                ? "I'll ask you some questions."
                : 'Je vais vous poser quelques questions.'}
            </Text>
          </CardItem>
        </Card>

        <Button onPress={() => this.props.navigation.navigate('Name')}>
          <Text>{this.props.system.language === 'en' ? 'Ok' : "D'accord"}</Text>
        </Button>
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const WelcomeRedux = connect(mapStateToProps, {})(Welcome);

export default WelcomeRedux;
