import React, { Component } from 'react';
import { Text, View, Keyboard } from 'react-native';
import { AppState } from '../store/store';
import { connect } from 'react-redux';
import { SystemState } from '../store/system/types';
import { NavigationProp, NavigationState } from '@react-navigation/native';
import { updateMenu, deleteMenu } from '../store/system/actions';
import { Menu } from '../entities';
import {
  List,
  ListItem,
  Button,
  Icon,
  Footer,
  H1,
  Spinner,
  Grid,
  Col,
  Row,
} from 'native-base';
import { FlatList } from 'react-native-gesture-handler';

interface MenuProps {
  system: SystemState;
  updateMenu: typeof updateMenu;
  deleteMenu: typeof deleteMenu;
  navigation: NavigationProp<
    Record<string, object>,
    string,
    NavigationState,
    {},
    {}
  >;
}

export class MenuList extends Component<MenuProps> {
  state = {
    isLoading: true,
  };

  renderMenu = ({ item }) => (
    <ListItem key={item.id}>
      <Button
        style={{ flex: 1 }}
        full
        light
        onPress={() =>
          this.props.navigation.navigate('MenuEdit', {
            id: item.id,
          })
        }
      >
        <Text>{item.title}</Text>
      </Button>
      <Button onPress={() => this.removeMenu(item)}>
        <Icon type="FontAwesome" name="trash"></Icon>
      </Button>
    </ListItem>
  );

  async componentDidMount() {
    const menus: Menu[] = this.props.system.menus;
    this.setState({ isLoading: false, menus: menus });
  }

  removeMenu(menu: Menu) {
    this.props.deleteMenu(menu);
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
        >
          <Spinner />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Grid style={{ minHeight: 80, maxHeight: 80 }} >
            <Row>
              <Col size={90}>
                <H1
                  style={{ paddingBottom: 15, paddingTop: 20, paddingLeft: 20 }}
                >
                  {this.props.system.language === 'en'
                    ? 'Your Kcal intakes'
                    : 'Vos apports en Kcal'}
                </H1>
              </Col>
              <Col size={20} style={{justifyContent:'center', alignContent: 'center'}}>
                <Button style={{justifyContent:'center'}}
                  icon transparent
                  onPress={() => this.props.navigation.navigate('MenuEdit')}
                >
                  <Icon fontSize={30} type="FontAwesome" name="plus" />
                </Button>
              </Col>
            </Row>
          </Grid>
          <FlatList
            data={this.props.system.menus}
            renderItem={this.renderMenu}
          />
        </View>
      );
    }
  }
}

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const MenuRedux = connect(mapStateToProps, { deleteMenu, updateMenu })(
  MenuList,
);

export default MenuRedux;
