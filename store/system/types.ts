import { Sex, HeightUnit, WeightUnit, User, Menu } from '../../entities';

export type Language = 'en' | 'fr';
export type ActivityType = 'sedentary' | 'light' | 'moderate' | 'high' | 'extreme';
export interface SystemState {
  loggedIn: boolean;
  user: User;
  language: Language
  userName: string | undefined;
  sex: Sex | undefined;
  dob: Date | undefined;
  activity: ActivityType | undefined;
  height: { value: number, unit: HeightUnit } | undefined;
  weight: { value: number, unit: WeightUnit } | undefined;
  formula: string | undefined;
  route: string | undefined;
  menus: Menu[];
}

// Describing the different ACTION NAMES available
export const UPDATE_LANGUAGE = 'UPDATE_LANGUAGE';
export const UPDATE_SEX = 'UPDATE_SEX';
export const UPDATE_DOB = 'UPDATE_DOB';
export const UPDATE_ACTIVITY = 'UPDATE_ACTIVITY';
export const UPDATE_NAME = 'UPDATE_NAME';
export const UPDATE_HEIGHT = 'UPDATE_HEIGHT';
export const UPDATE_WEIGHT = 'UPDATE_WEIGHT';
export const UPDATE_FORMULA = 'UPDATE_FORMULA';
export const UPDATE_ROUTE = 'UPDATE_ROUTE';
export const UPDATE_MENU = 'UPDATE_MENU';
export const DELETE_MENU = 'DELETE_MENU';
export const CLEAR_ALL_DATA = 'CLEAR_ALL_DATA';

// UPDATE_ACTIVITY UPDATE_NAME UPDATE_HEIGHT UPDATE_WEIGHT

interface UpdateLanguageAction {
  type: typeof UPDATE_LANGUAGE;
  payload: Language;
}

interface UpdateSexAction {
  type: typeof UPDATE_SEX;
  payload: Sex;
}

interface UpdateDobAction {
  type: typeof UPDATE_DOB;
  payload: Date;
}

interface UpdateActivityAction {
  type: typeof UPDATE_ACTIVITY;
  payload: ActivityType;
}

interface ClearAllDataAction {
  type: typeof CLEAR_ALL_DATA;
}

interface UpdateNameAction {
  type: typeof UPDATE_NAME;
  payload: string;
}

interface UpdateHeightAction {
  type: typeof UPDATE_HEIGHT;
  payload: { value: number, unit: HeightUnit };
}

interface UpdateWeightAction {
  type: typeof UPDATE_WEIGHT;
  payload: { value: number, unit: WeightUnit };
}

interface UpdateFormulaAction {
  type: typeof UPDATE_FORMULA;
  payload: string;
}

interface UpdateRouteAction {
  type: typeof UPDATE_ROUTE;
  payload: string;
}


interface UpdateMenuAction {
  type: typeof UPDATE_MENU;
  payload: {
    menu: Menu, isNew: boolean
  };
}


interface DeleteMenuAction {
  type: typeof DELETE_MENU;
  payload: Menu;
}

export type SystemActionTypes =
  UpdateLanguageAction |
  UpdateSexAction |
  UpdateDobAction |
  UpdateActivityAction |
  UpdateNameAction |
  UpdateHeightAction |
  UpdateWeightAction |
  UpdateFormulaAction |
  UpdateRouteAction |
  UpdateMenuAction |
  DeleteMenuAction |
  ClearAllDataAction;
