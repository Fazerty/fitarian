import { UPDATE_LANGUAGE, UPDATE_SEX, Language, UPDATE_DOB, UPDATE_ACTIVITY, UPDATE_NAME, UPDATE_HEIGHT, UPDATE_WEIGHT, ActivityType, UPDATE_ROUTE, CLEAR_ALL_DATA, UPDATE_FORMULA, UPDATE_MENU, DELETE_MENU } from './types';
import { Sex, HeightUnit, WeightUnit, Menu } from '../../entities';

export function updateLanguage(newLanguage: Language) {
  return {
    type: UPDATE_LANGUAGE,
    payload: newLanguage
  };
}

export function updateSex(newSex: Sex) {
  return {
    type: UPDATE_SEX,
    payload: newSex
  };
}

export function updateDob(newDate: Date) {
  return {
    type: UPDATE_DOB,
    payload: newDate
  };
}

export function updateActivity(newActivity: ActivityType) {
  return {
    type: UPDATE_ACTIVITY,
    payload: newActivity
  };
}

export function clearAllData() {
  return {
    type: CLEAR_ALL_DATA,
  };
}



export function updateName(newName: string) {
  return {
    type: UPDATE_NAME,
    payload: newName
  };
}

export function updateHeight(newHeight: { value: number, unit: HeightUnit }) {
  return {
    type: UPDATE_HEIGHT,
    payload: newHeight
  };
}

export function updateWeight(newWeight: { value: number, unit: WeightUnit }) {
  return {
    type: UPDATE_WEIGHT,
    payload: newWeight
  };
}

export function updateFormula(newFormula: string) {
  return {
    type: UPDATE_FORMULA,
    payload: newFormula
  };
}

export function updateRoute(newRoute: string) {
  return {
    type: UPDATE_ROUTE,
    payload: newRoute
  };
}

export function deleteMenu(menu: Menu) {
  return {
    type: DELETE_MENU,
    payload: menu
  };
}

export function updateMenu(menu: Menu, isNew: boolean = false) {
  return {
    type: UPDATE_MENU,
    payload: { menu, isNew }
  };
}