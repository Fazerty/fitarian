import { UPDATE_LANGUAGE, SystemState, SystemActionTypes, UPDATE_SEX, UPDATE_NAME, UPDATE_DOB, UPDATE_HEIGHT, UPDATE_WEIGHT, UPDATE_ACTIVITY, UPDATE_ROUTE, CLEAR_ALL_DATA, Language, UPDATE_FORMULA, DELETE_MENU, UPDATE_MENU } from './types';
import * as Localization from 'expo-localization';
import { userService, menuService } from '../../App';
import { User, Menu } from '../../entities';
import { MenuService } from '../../services/menuService';



const initialState: SystemState = {
  loggedIn: false,
  language: Localization.locale.startsWith('en-') ? 'en' : 'fr',
  user: undefined, // Used to retreive data of the user when the app is opened
  userName: undefined,
  sex: undefined,
  dob: undefined,
  activity: undefined,
  height: undefined,
  weight: undefined,
  formula: undefined,
  route: undefined,
  menus: []
};

export function systemReducer(
  state = initialState,
  action: SystemActionTypes
): SystemState {

  switch (action.type) {
    case UPDATE_LANGUAGE: {
      return {
        ...state,
        language: action.payload
      };
    }
    case UPDATE_SEX: {
      state.user.sex = action.payload;
      userService.updateUser(state.user);
      return {
        ...state,
        sex: action.payload
      };
    }
    case UPDATE_NAME: {
      state.user.name = action.payload;
      userService.updateUser(state.user);
      return {
        ...state,
        userName: action.payload
      };
    }
    case UPDATE_FORMULA: {
      state.user.formula = action.payload;
      userService.updateUser(state.user);
      return {
        ...state,
        formula: action.payload
      };
    }
    case UPDATE_DOB: {
      state.user.dateOfBirth = action.payload;
      userService.updateUser(state.user);
      return {
        ...state,
        dob: action.payload
      };
    }
    case UPDATE_HEIGHT: {
      state.user.heightUnit = action.payload.unit;
      state.user.heightValue = action.payload.value;
      userService.updateUser(state.user);
      return {
        ...state,
        height: action.payload
      };
    }
    case UPDATE_WEIGHT: {
      state.user.expectedWeightUnit = action.payload.unit;
      state.user.expectedWeightValue = action.payload.value;
      userService.updateUser(state.user);
      return {
        ...state,
        weight: action.payload
      };
    }
    case UPDATE_ACTIVITY: {
      userService.setUserFirstActivity(state.user, action.payload);
      return {
        ...state,
        activity: action.payload
      };
    }
    case CLEAR_ALL_DATA: {
      userService.deleteUser(state.user);
      menuService.deleteMenus();

      const newState = {
        loggedIn: false,
        language: (Localization.locale.startsWith('en-') ? 'en' : 'fr') as Language,
        user: new User(),
        userName: undefined,
        sex: undefined,
        dob: undefined,
        activity: undefined,
        height: undefined,
        weight: undefined,
        formula: undefined,
        route: undefined,
        menus: []
      }
      userService.getUser().then((value: User) => { state.user = value });
      return newState;
    }

    case UPDATE_ROUTE: {
      return {
        ...state,
        route: action.payload
      };
    }

    case DELETE_MENU: {
      menuService.deleteMenu(action.payload);
      const newMenus = [...state.menus];
      newMenus.splice(newMenus.map((menu: Menu) => menu.id).indexOf(action.payload.id), 1);
      return {
        ...state,
        menus: newMenus
      };
    }

    case UPDATE_MENU: {
      if (!action.payload.isNew) {
        // Not necessary if menu is new. It has just been created.
        menuService.updateMenu(action.payload.menu)
      }
      const newMenus = [...state.menus];
      if (action.payload.isNew) {
        newMenus.push(action.payload.menu);
      } else {
        newMenus.splice(newMenus.map((menu: Menu) => menu.id).indexOf(action.payload.menu.id), 1, action.payload.menu)
      }
      return {
        ...state,
        menus: newMenus
      };
    }

    default:
      return state;
  }
}
