import 'reflect-metadata';
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { YellowBox } from 'react-native';
import { ApolloProvider } from '@apollo/react-hooks';
import { initApolloClient, apolloState } from './utils/apollo';
import { Provider } from 'react-redux';
import { reduxStore } from './utils/redux';
import Router from './components/common/router.component';
import { UserService } from './services/userService';
import { connect } from './utils/database';
import { SystemState } from './store/system/types';
import { User, Menu } from './entities';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import getTheme from './native-base-theme/components';
import { StyleProvider, Container } from 'native-base';
import AskFooter from './components/ask/progressionFooter.component';
import { MenuService } from './services/menuService';
import { AlimService } from './services/alimService';
import { Connection } from 'typeorm/browser';

YellowBox.ignoreWarnings(['Require cycle:']);
console.ignoredYellowBox = ['Require cycle:'];

export let userService: UserService;
export let menuService: MenuService;
export let alimService: AlimService;

export default class App extends Component {
  state = {
    isReady: false,
    hasAllData: false,
  };

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    let connection: Connection;
    try {
      connection = await connect();
    } catch (error) {
      console.log(error);
    }
    userService = new UserService();
    menuService = new MenuService();
    alimService = new AlimService();

    const systemState: SystemState = reduxStore.getState().system;

    const user: User = await userService.getUser();
    systemState.user = user;
    systemState.sex = user.sex;
    systemState.dob = user.dateOfBirth;
    systemState.userName = user.name;
    systemState.formula = user.formula;
    systemState.activity = await userService.getFirstActivity(user);
    systemState.height = { value: user.heightValue, unit: user.heightUnit };
    systemState.weight = { value: user.expectedWeightValue, unit: user.expectedWeightUnit };

    const menus: Menu[] = await menuService.getMenus();

    systemState.menus = menus;
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  private client = initApolloClient(apolloState);

  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    return (
      <Provider store={reduxStore}>
        <ApolloProvider client={this.client}>
          <StyleProvider style={getTheme()}>
            <Container style={{ flex: 1 }}>
              <Router />
              <AskFooter></AskFooter>
            </Container>
          </StyleProvider>
        </ApolloProvider>
      </Provider>
    );
  }
}
