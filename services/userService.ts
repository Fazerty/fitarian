import { getCustomRepository } from "typeorm/browser";
import { User, WeightUnit, ActivityInfo, WeightInfo } from "../entities";
import { UserRepository } from "../repository/userRepository";
import { ActivityInfoRepository } from "../repository/activityInfoRepository";
import { WeightInfoRepository } from "../repository/weightInfoRepository";
import { ActivityType } from "../store/system/types";
import { DeleteResult } from "typeorm";

export class UserService {
  private userRepository!: UserRepository;
  private activityRepository: ActivityInfoRepository;
  private weightInfoRepository: WeightInfoRepository;

  constructor() {
    this.userRepository = getCustomRepository(UserRepository);
    this.activityRepository = getCustomRepository(ActivityInfoRepository);
    this.weightInfoRepository = getCustomRepository(WeightInfoRepository);
  }



  /**
   * Returns the user stored in db if exists.
   * If not, it creates a new user and stores it in the db.
   *
   * @returns {Promise<User>}
   * @memberof UserService
   */
  async getUser(): Promise<User> {
    let user: User | undefined = await this.userRepository.findOne();
    if (!user) {
      const newUser = new User();
      user = await this.userRepository.save(newUser);
      return user;
    }
    return user;
  }

  /**
   * Saves the user to db.
   *
   * @param {User} user
   * @param {(Sex | undefined)} sex
   * @returns {Promise<User>}
   * @memberof UserService
   */
  async updateUser(user: User): Promise<User> {
    return await this.userRepository.save(user);
  }


  /**
   * Deletes the user from db.
   *
   * @param {User} user
   * @returns {Promise<DeleteResult>}
   * @memberof UserService
   */
  async deleteUser(_user: User): Promise<void> {
    return await this.userRepository.clear();
  }

  /**
   * Gets the last recorded weight
   *
   * @param {*} user
   * @returns {(Promise<{value: number, unit:WeightUnit} | undefined>)}
   * @memberof UserService
   */
  async getLastWeight(user: User): Promise<{ value: number, unit: WeightUnit } | undefined> {
    const weight: WeightInfo | undefined = await this.weightInfoRepository.findLast(user);
    if (weight) {
      return { value: weight.weightValue, unit: weight.weightUnit }
    }
  };

  /**
   * Gets the last recorded activity as a string.
   *
   * @param {*} user
   * @returns {(Promise<string | undefined>)}
   * @memberof UserService
   */
  async getFirstActivity(user: User): Promise<ActivityType | undefined> {
    const activity: ActivityInfo | undefined = await this.activityRepository.findFirst(user);
    if (activity) {
      return activity.activity;
    }

  };

  /**
   *
   *
   * @param {User} user
   * @param {Activity} activityType
   * @memberof UserService
   */
  async setUserFirstActivity(user: User, activityType: ActivityType) {
    let activity: ActivityInfo | undefined = await this.activityRepository.findFirst(user);
    if (!activity) {
      activity = new ActivityInfo();
      activity.date = new Date();
      activity.user = user;
    }
    activity.activity = activityType;
    this.activityRepository.save(activity);
  };

}