import { getCustomRepository } from "typeorm/browser";
import { Menu } from "../entities";
import { MenuRepository } from "../repository/menuRepository";
import { DeleteResult } from "typeorm";

export class MenuService {

  private menuRepository: MenuRepository = getCustomRepository(MenuRepository);

  /**
   * Returns the menus ordered by creation date desc.
   *
   * @returns {Promise<Menu[]>}
   * @memberof MenuService
   */
  async getMenus(): Promise<Menu[]> {
    return await this.menuRepository.find({ order: { date: 'DESC' } });;
  }

  /**
   * Returns a menu with given id if exists. If not creates a new onewith 'Menu' as default title.
   *
   * @returns {Promise<Menu>}
   * @memberof MenuService
   */
  async getMenu(id: number | undefined, language: 'en' | 'fr'): Promise<Menu> {
    let menu: Menu | undefined = id ? await this.menuRepository.findOne({ where: { id }, relations: ['ingredients'] }) : undefined;
    if (!menu) {
      const newMenu: Menu = new Menu();
      const currDate: Date = new Date();
      newMenu.title = language === 'en' ? 'Intake of ' + currDate.toLocaleDateString('en') : 'Apport du ' + currDate.toLocaleDateString('fr') ;
      newMenu.date = new Date();
      menu = await this.menuRepository.save(newMenu);
      menu.ingredients = [];
    }
    return menu;
  }

  /**
   * Saves the Menu to db.
   *
   * @param {Menu} menu
   * @param {(Sex | undefined)} sex
   * @returns {Promise<Menu>}
   * @memberof MenuService
   */
  async updateMenu(menu: Menu): Promise<Menu> {
    return await this.menuRepository.save(menu);
  }


  /**
   * Deletes a menu from db.
   *
   * @param {menu} Menu
   * @returns {Promise<DeleteResult>}
   * @memberof MenuService
   */
  async deleteMenu(menu: Menu): Promise<DeleteResult> {
    return await this.menuRepository.delete(menu.id);
  }

  /**
 * Deletes all menus from db.
 *
 * @param {menu} Menu
 * @returns {Promise<void>}
 * @memberof MenuService
 */
  async deleteMenus(): Promise<void> {
    return await this.menuRepository.clear();
  }

}