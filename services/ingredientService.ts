import { getCustomRepository } from "typeorm/browser";
import { Ingredient } from "../entities";
import { IngredientRepository } from "../repository/ingredientRepository";
import { DeleteResult } from "typeorm";

export class IngredientService {

  private ingredientRepository: IngredientRepository = getCustomRepository(IngredientRepository);


  /**
   * Saves the Ingredient to db.
   *
   * @param {Ingredient} Ingredient
   * @param {(Sex | undefined)} sex
   * @returns {Promise<Ingredient>}
   * @memberof IngredientService
   */
  async updateIngredient(Ingredient: Ingredient): Promise<Ingredient> {
    return await this.ingredientRepository.save(Ingredient);
  }


  /**
   * Deletes a Ingredient from db.
   *
   * @param {Ingredient} Ingredient
   * @returns {Promise<DeleteResult>}
   * @memberof IngredientService
   */
  async deleteIngredient(Ingredient: Ingredient): Promise<DeleteResult> {
    return await this.ingredientRepository.delete(Ingredient.id);
  }

  /**
 * Deletes all Ingredients from db.
 *
 * @param {Ingredient} Ingredient
 * @returns {Promise<void>}
 * @memberof IngredientService
 */
  async deleteIngredients(): Promise<void> {
    return await this.ingredientRepository.clear();
  }

}