import { getCustomRepository } from "typeorm/browser";
import { Alim, Compo } from "../entities";
import { AlimRepository } from "../repository/alimRepository";
import { SaveOptions } from "typeorm";

export class AlimService {

  private alimRepository: AlimRepository = getCustomRepository(AlimRepository);

  /**
   * Checks if the alim already exists in the db. If exits returns it.
   * If it doesn't exists save it in the db and returns it.
   *
   * @private
   * @param {Alim} alim
   * @memberof AlimService
   */
  public async saveAlim(alim: Alim): Promise<Alim> {
    const exisitingAlim: Alim | undefined = await this.alimRepository.findOne({ alim_code: alim.alim_code })

    if (exisitingAlim) {
      return exisitingAlim;
    }
    const ligthAlim = { ...alim }
    /*
      const_code: 327 => Energy, Regulation EU No 1169/2011 (kJ/100g)
      const_code: 328 => Energy, Regulation EU No 1169/2011 (kcal/100g)
      const_code: 332  => Energy, N x Jones&apos; factor, with fibres (kJ/100g)
      const_code: 333 => Energy, N x Jones&apos; factor, with fibres  (kcal/100g)
    */
    ligthAlim.compos = ligthAlim.compos.filter((compo: Compo) => {
      if (compo.const_code === 328) { return true; }
    })
    const newAlim: Alim = await this.alimRepository.save(ligthAlim);
    // Takes several seconds to save full alim => splitted in 2 steps.
    this.alimRepository.save(alim);
    return newAlim;
  }

}