import { EntityRepository, Repository } from 'typeorm';
import { WeightInfo, User } from '../entities';

/**
 * repository for the WeightInfo entity.
 */
@EntityRepository(WeightInfo)
export class WeightInfoRepository extends Repository<WeightInfo> {

  /**
   * Finds the last weight of a user (by date)
   *
   * @param {User} user
   * @returns {Promise<WeightInfo>}
   * @memberof WeightInfoRepository
   */
  findLast(user: User): Promise<WeightInfo> {
    return this.findOne({ where: { user }, order: { date: 'DESC' } })
  }

  /**
   * Finds the first weight of a user (by date)
   *
   * @param {User} user
   * @returns {Promise<WeightInfo>}
   * @memberof WeightInfoRepository
   */
  findFirst(user: User): Promise<WeightInfo> {
    return this.findOne({ where: { user }, order: { date: 'ASC' } })
  }
}