import { EntityRepository, Repository } from 'typeorm';
import {Menu} from '../entities';

/**
 * repository for the Menu entity.
 */
@EntityRepository(Menu)
export class MenuRepository extends Repository<Menu> {

}