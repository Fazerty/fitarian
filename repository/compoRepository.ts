import { EntityRepository, Repository } from 'typeorm';
import {Compo} from '../entities';

/**
 * repository for the Compo entity.
 */
@EntityRepository(Compo)
export class CompoRepository extends Repository<Compo> {

}