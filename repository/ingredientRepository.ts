import { EntityRepository, Repository } from 'typeorm';
import {Ingredient} from '../entities';

/**
 * repository for the Ingredient entity.
 */
@EntityRepository(Ingredient)
export class IngredientRepository extends Repository<Ingredient> {

}