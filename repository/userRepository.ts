import { EntityRepository, Repository } from 'typeorm';
import {User} from '../entities';

/**
 * repository for the User entity.
 */
@EntityRepository(User)
export class UserRepository extends Repository<User> {

}