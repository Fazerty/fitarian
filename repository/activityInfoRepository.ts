import { EntityRepository, Repository } from 'typeorm';
import {ActivityInfo, User} from '../entities';

/**
 * repository for the ActivityInfo entity.
 */
@EntityRepository(ActivityInfo)
export class ActivityInfoRepository extends Repository<ActivityInfo> {

  /**
   * Finds the first activity of a user (by date)
   *
   * @param {User} user
   * @returns {Promise<ActivityInfo>}
   * @memberof ActivityInfoRepository
   */
  findFirst(user: User): Promise<ActivityInfo> {
      return this.findOne({where: {user}, order: {date: 'ASC'}})
  }

  /**
   * Finds the last activity of a user (by date)
   *
   * @param {User} user
   * @returns {Promise<ActivityInfo>}
   * @memberof ActivityInfoRepository
   */
  findLast(user: User): Promise<ActivityInfo> {
    return this.findOne({where: {user}, order: {date: 'DESC'}})
  }
}