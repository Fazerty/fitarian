import React, { Component } from 'react';
import { Footer, Button, FooterTab, Icon } from 'native-base';
import { askRoutePositionAndNext, askRoutes } from '../common/router.component';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import { updateRoute } from '../../store/system/actions';
import { SystemState } from '../../store/system/types';

interface AskFooterProps {
  system: SystemState;
  updateRoute: typeof updateRoute;
}

export class AskFooter extends Component<AskFooterProps> {



  render() {
    if (this.props.system.route) {

      const routeInfo: {
        position: number;
        nextRoute: string | undefined;
        total: number;
      } = askRoutePositionAndNext(this.props.system.route);
      const buttons = [];
      if (routeInfo.position != -1)
      {for (let i = 0; i < routeInfo.total; i++) {
        buttons.push(
          <Button disabled={i === routeInfo.position}
            // onPress={() => { this.props.updateRoute(askRoutes[i]); }} //
            key={i}>
            <Icon
              type='FontAwesome'
              name={i < routeInfo.position ? 'circle' : 'circle-o'}
            />
          </Button>,
        );
      }

      return (
        <Footer>
          <FooterTab>{buttons}</FooterTab>
        </Footer>
      );
      } else return null;

    } else return null;
  }
}

// Connect the component to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};
const AskFooterRedux = connect(mapStateToProps, { updateRoute })(AskFooter);

export default AskFooterRedux;
