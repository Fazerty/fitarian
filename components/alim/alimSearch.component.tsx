import React from 'react'; // For jest
import { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from '@apollo/react-components';
import { AlimResult } from '../../entities/alimResult';
import { TextInput, View, StyleSheet, Text, Keyboard } from 'react-native';
import { Alim } from '../../entities';
import {
  ListItem,
  List,
  Card,
  CardItem,
  Grid,
  Col,
  Button,
  Spinner,
  Body,
  Right,
  Icon,
} from 'native-base';

const styles = StyleSheet.create({
  alimSearch: {
    flex: 1,
  },
  title: {},
  subtitle: {},
  pagination: {},
  pageInfo: {},
  resultList: {
    backgroundColor: 'lightgreen',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  results: {
    flex: 1,
  },
  searchField: {
    flexDirection: 'row',
    borderColor: 'gray',
    borderWidth: 1,
  },
});

interface Props {
  language: 'en' | 'fr';
  onSelect: any;
}

export class AlimSearch extends Component<Props> {
  state: {
    search: string;
    language: string;
    limit: number;
    offset: number;
    selectedAlimId: number | undefined;
  } = {
    search: '',
    language: this.props.language,
    limit: 10,
    offset: 0,
    selectedAlimId: undefined,
  };

  render() {
    const { search, offset } = this.state;

    function selectAlim(data: Partial<Alim>) {
      this.props.onSelect(data);
    }
    /**
     * Query to retreive alims based on a search string.
     * Fields for name are included depending on the language.
     *
     * Rem:
     * The english argument should be removed.
     * alim_nom_eng and alim_nom_fr should be replaced by a field alim_nom (the name in the selected language).
     *
     */
    const QUERY = gql`
      query GetAlims($search: String!, $lang: String!, $offset: Int!) {
        alims(search: $search, lang: $lang, offset: $offset) {
          result {
            id
            alim_code
            alim_nom_eng
            alim_nom_fr
          }
          count
        }
      }
    `;

    /**
     * Query to retreive full alim info by alim id.
     *
     *
     */
    const QUERYINFO = gql`
      query GetAlim($id: Int!) {
        alim(id: $id) {
          alim_code
          alim_nom_eng
          alim_nom_fr
          compos {
            const_code
            const {
              const_nom_fr
              const_nom_eng
            }
            teneur
            min
            max
            code_confiance
          }
        }
      }
    `;

    if (this.state.selectedAlimId) {
      // Should find a better way to query the full info.
      // Maybe change this class component to a functional component and use useLazyQuery hook.
      return (
        <Query<any, any>
          query={QUERYINFO}
          variables={{
            id: this.state.selectedAlimId,
          }}
          onError={(_error: any) => {}}
        >
          {({ data }: { data: any }) => {
            if (data) {
              const { alim }: { alim: Alim } = data;
              this.props.onSelect(alim);
              return (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text>
                    {this.state.language === 'en'
                      ? 'Importing data...'
                      : 'Importation des données...'}
                  </Text>
                  <Spinner />
                </View>
              );
            }
          }}
        </Query>
      );
    } else
      return (
        <Query<any, any>
          query={QUERY}
          variables={{
            search,
            lang: this.state.language,
            offset,
          }}
          onError={(_error: any) => {}}
        >
          {({ data }: { data: any }) => {
            if (data) {
              const { alims }: { alims: AlimResult } = data;
              return (
                <View style={styles.alimSearch}>
                  <Card>
                    <CardItem>
                      {/* Search field */}
                      <View style={styles.searchField}>
                        <TextInput
                          style={{
                            flex: 1,
                            height: 40,
                            borderColor: 'gray',
                            paddingLeft: 10,
                          }}
                          value={this.state.search}
                          onChangeText={(text) =>
                            this.setState({ search: text })
                          }
                          placeholder={
                            this.state.language === 'en'
                              ? 'Search'
                              : 'Recherche'
                          }
                        ></TextInput>

                        <Button
                          icon hasText
                          transparent
                          style={{alignItems:'center', justifyContent: 'flex-end'}}
                          onPress={() => {
                            Keyboard.dismiss();
                          }}
                        >
                          <Icon fontSize={10} type="FontAwesome" name="search" />
                        </Button>
                      </View>
                    </CardItem>

                    {/* Results*/}
                    <CardItem>
                      <List style={styles.results}>
                        {alims.result.map((value: Partial<Alim>) => {
                          return (
                            <ListItem icon key={value.alim_code}>
                              <Body>
                                <Text>
                                  {this.state.language === 'en'
                                    ? value.alim_nom_eng
                                    : value.alim_nom_fr}
                                </Text>
                              </Body>
                              <Right>
                                <Button
                                  icon
                                  transparent
                                  small
                                  onPress={() => {
                                    this.setState({ selectedAlimId: value.id });
                                  }}
                                >
                                  <Icon type="FontAwesome" name="plus" />
                                </Button>
                              </Right>
                            </ListItem>
                          );
                        })}
                      </List>
                    </CardItem>

                    {/* Page information */}
                    <CardItem style={{ justifyContent: 'center' }}>
                      <Text style={{ fontSize: 18 }}>
                        {Math.floor(offset / this.state.limit) + 1}/
                        {Math.floor(alims.count / this.state.limit) + 1}
                      </Text>
                    </CardItem>

                    {/* Pagination */}
                    <CardItem>
                      <Grid>
                        <Col>
                          <Button
                            icon
                            small
                            style={{ justifyContent: 'center' }}
                            disabled={offset === 0}
                            color="#841584"
                            onPress={() => this.setState({ offset: 0 })}
                          >
                            <Icon type="FontAwesome" name="angle-double-left" />
                          </Button>
                        </Col>
                        <Col>
                          <Button
                            icon
                            small
                            style={{ justifyContent: 'center' }}
                            disabled={offset === 0}
                            color="#841584"
                            onPress={() =>
                              this.setState({
                                offset: offset - 1 * this.state.limit,
                              })
                            }
                          >
                            <Icon type="FontAwesome" name="angle-left" />
                          </Button>
                        </Col>
                        <Col>
                          <Button
                            icon
                            small
                            style={{ justifyContent: 'center' }}
                            disabled={offset >= alims.count - this.state.limit}
                            color="#841584"
                            onPress={() =>
                              this.setState({
                                offset: offset + 1 * this.state.limit,
                              })
                            }
                          >
                            <Icon type="FontAwesome" name="angle-right" />
                          </Button>
                        </Col>
                        <Col>
                          <Button
                            icon
                            small
                            style={{ justifyContent: 'center' }}
                            color="#841584"
                            disabled={offset >= alims.count - this.state.limit}
                            onPress={() =>
                              this.setState({
                                offset:
                                  Math.floor(alims.count / this.state.limit) *
                                  this.state.limit,
                              })
                            }
                          >
                            <Icon
                              type="FontAwesome"
                              name="angle-double-right"
                            />
                          </Button>
                        </Col>
                      </Grid>
                    </CardItem>
                  </Card>
                </View>
              );
            }
            return (
              <View style={styles.alimSearch}>
                <Card>
                  <CardItem>
                    {/* Search field */}
                    <TextInput
                      style={{
                        flex: 1,
                        height: 40,
                        borderColor: 'gray',
                        borderWidth: 1,
                        paddingLeft: 10,
                      }}
                      value={this.state.search}
                      onChangeText={(text) => this.setState({ search: text })}
                      placeholder={
                        this.state.language === 'en' ? 'Search' : 'Recherche'
                      }
                    ></TextInput>
                  </CardItem>
                </Card>
              </View>
            );
          }}
        </Query>
      );
  }
}

export default AlimSearch;
