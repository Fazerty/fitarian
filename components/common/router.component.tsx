import React, { Component } from 'react';
import { NavigationContainer, NavigationState } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Welcome from '../../views/welcomeView';
import AskSex from '../../views/questions/askSexView';
import AskDateOfBirth from '../../views/questions/askDateOfBirthView';
import AskName from '../../views/questions/askNameView';
import AskHeight from '../../views/questions/askHeightView';
import AskWeight from '../../views/questions/askWeightView';
import AskFormula from '../../views/questions/askFormulaView';
import { SystemState } from '../../store/system/types';
import { AppState } from '../../store/store';
import { connect } from 'react-redux';
import AskActivity from '../../views/questions/askActivityView';
import MenuList from '../../views/menuListView';
import MenuEdit from '../../views/menuEditView';
import { updateRoute } from '../../store/system/actions';
import { Button, Icon, View } from 'native-base';
import { StyleSheet } from 'react-native';
import ClearData from '../../views/clearView';
import About from '../../views/aboutView';

import AlimSearch from '../alim/alimSearch.component';

const Stack = createStackNavigator();

interface RouterProps {
  system: SystemState;
  updateRoute: typeof updateRoute;
}

interface RootProps {
  system: SystemState;
  updateRoute: typeof updateRoute;
  navigation: any;
}

/**
 * The routes to ask questions to user in order.
 *
 */
export const askRoutes: string[] = [
  'Name',
  'Sex',
  'Dob',
  'Height',
  'Weight',
  'Activity',
  'Formula',
];

/**
 * Returns the current route position in routes to ask questions to user and the next route if exists.
 *
 * @export
 * @param {string} currRoute
 * @returns {({ position: number; nextRoute: string | undefined })}
 */
export function askRoutePositionAndNext(
  currRoute: string,
): { position: number; nextRoute: string | undefined; total: number } {
  const position: number = askRoutes.indexOf(currRoute);
  let nextRoute: string | undefined;
  if (position < askRoutes.length - 1) {
    nextRoute = askRoutes[position + 1];
  }
  return { position, nextRoute, total: askRoutes.length };
}

export class Root extends Component<RootProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Stack.Navigator
        initialRouteName="Welcome"
        screenOptions={{
          headerStyle: {
            backgroundColor: 'green',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      >
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{
            title:
              this.props.system.language === 'en' ? 'Welcome' : 'Bienvenue',
          }}
        />
        <Stack.Screen
          name="Name"
          component={AskName}
          options={{
            title:
              this.props.system.language === 'en' ? 'Your name' : 'Votre Nom',
          }}
        />
        <Stack.Screen
          name="Sex"
          component={AskSex}
          options={{
            title:
              this.props.system.language === 'en' ? 'Your sex' : 'Votre sexe',
          }}
        />
        <Stack.Screen
          name="Dob"
          component={AskDateOfBirth}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'Your Date of birth'
                : 'Votre date de naissance',
          }}
        />
        <Stack.Screen
          name="Height"
          component={AskHeight}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'Your height'
                : 'Votre taille',
          }}
        />
        <Stack.Screen
          name="Weight"
          component={AskWeight}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'Your weight'
                : 'Votre poids',
          }}
        />
        <Stack.Screen
          name="Activity"
          component={AskActivity}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'Your activity'
                : 'Votre activité',
          }}
        />
        <Stack.Screen
          name="Formula"
          component={AskFormula}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'Formula to use'
                : 'La formule à utiliser',
          }}
        />
      </Stack.Navigator>
    );
  }
}

export class Calculator extends Component<RootProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'green',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      >
        <Stack.Screen
          name="MenuList"
          component={MenuList}
          options={{
            title: this.props.system.language === 'en' ? 'Kcal intake by day' : 'Apport en kcal par jour',
            headerRight: () => (
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}
                style={{}}
              >
                <Icon type="FontAwesome" name="ellipsis-v" />
              </Button>
            ),
          }}
        />
        <Stack.Screen
          name="MenuEdit"
          component={MenuEdit}
          options={{
            title: this.props.system.language === 'en' ? 'Edit the Kcal intake' : 'Edition de l\'apport en Kcal',
            headerRight: () => (
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}
                style={{}}
              >
                <Icon type="FontAwesome" name="ellipsis-v" />
              </Button>
            ),
          }}
        />
      </Stack.Navigator>
    );
  }
}

export class ClearMenu extends Component<RootProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'green',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => <View></View>,
        }}
      >
        <Stack.Screen
          name="Clear"
          component={ClearData}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'Clear Data'
                : 'Effacer les données',
            headerRight: () => (
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}
                style={{}}
              >
                <Icon type="FontAwesome" name="ellipsis-v" />
              </Button>
            ),
          }}
        />
      </Stack.Navigator>
    );
  }
}

export class AboutMenu extends Component<RootProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'green',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => <View></View>,
        }}
      >
        <Stack.Screen
          name="About"
          component={About}
          options={{
            title:
              this.props.system.language === 'en'
                ? 'About'
                : 'A propos',
            headerRight: () => (
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}
                style={{}}
              >
                <Icon type="FontAwesome" name="ellipsis-v" />
              </Button>
            ),
          }}
        />
      </Stack.Navigator>
    );
  }
}
export class Router extends Component<RouterProps> {
  state = {};

  constructor(props) {
    super(props);
  }

  hasAllData(): boolean {
    if (
      this.props.system.user &&
      this.props.system.sex &&
      this.props.system.dob &&
      this.props.system.userName &&
      this.props.system.activity &&
      this.props.system.height &&
      this.props.system.height.value &&
      this.props.system.weight &&
      this.props.system.weight.value
    ) {
      return true;
    } else {
      return false;
    }
  }
  render() {
    const Drawer = createDrawerNavigator();

    return (
      <View style={{ flex: 1 }}>
        <NavigationContainer
          onStateChange={(state) => {
            const subState: NavigationState | Partial<NavigationState> =
              state.routes[state.index].state;
            if (
              subState &&
              subState.routes[subState.index].name !== this.props.system.route
            ) {
              this.props.updateRoute(subState.routes[subState.index].name);
            }
          }}
        >
          <Drawer.Navigator
            initialRouteName={this.hasAllData() ? 'Calculator' : 'Params'}
          >
            <Drawer.Screen
              name="Params"
              component={RootRedux}
              options={{
                title:
                  this.props.system.language === 'en' ? 'Params' : 'Params',
              }}
            />
            <Drawer.Screen
              name="Clear"
              component={ClearRedux}
              options={{
                title:
                  this.props.system.language === 'en'
                    ? 'Clear Data'
                    : 'Effacer les données',
              }}
            />
            {this.hasAllData() && (
              <Drawer.Screen
                name="Calculator"
                component={CalcRedux}
                options={{
                  title:
                    this.props.system.language === 'en'
                      ? 'Kcal Calculator'
                      : 'KCal Calculatrice',
                }}
              />
            )}
            <Drawer.Screen
              name="About"
              component={AboutRedux}
              options={{
                title:
                  this.props.system.language === 'en'
                    ? 'About'
                    : 'A propos',
              }}
            />
          </Drawer.Navigator>
        </NavigationContainer>
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

// Connect the components to the redux state
const mapStateToProps = (state: AppState) => {
  return {
    system: state.system,
  };
};

const RootRedux = connect(mapStateToProps, { updateRoute })(Root);

const CalcRedux = connect(mapStateToProps, { updateRoute })(Calculator);

const ClearRedux = connect(mapStateToProps, { updateRoute })(ClearMenu);
const AboutRedux = connect(mapStateToProps, { updateRoute })(AboutMenu);

const RouterRedux = connect(mapStateToProps, { updateRoute })(Router);

export default RouterRedux;
